all: local docker

build: dist handout
local: clean build dist handout test
docker: docker-clean docker-build docker-test

compile:
	# Compile/build any of the things you
	# need for the challenge
	mkdir -p build
	gcc -o build/chal -I include src/chal.c

dist: compile
	mkdir -p dist
	# Copy things that are nexessary on
	# the challenge server to dist
	cp build/chal dist/chal
	cp flag.txt dist/flag.txt

handout: compile
	mkdir -p handout
	# Copy things you want to give to players
	# into the handout directory
	cp build/chal handout/chal
	strip handout/chal

run: dist
	# This runs the challenge, so we just need
	# stuff from dist.
	./run.sh

test: dist handout
	# Use the stuff in handout and dist to
	# solve the challenge
	./solve.py

# --- Docker releated items ---
CHALLENGE_NAME = <name>
CHALLENGE_CATEGORY = <category>
CI_REGISTRY_IMAGE := registry.gitlab.com/cybears-private/fall-of-cybeartron
CHALLENGE_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}
BUILD_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}-builder
HEALTHCHECK_IMAGE_TAG = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}-healthcheck
REPO_DIR := $(shell git rev-parse --show-toplevel)
CHALLENGE_HOST = ${CHALLENGE_NAME}.ctf.cybears.io
CHALLENGE_PORT = 2323
CHALLENGE_NETWORK = ${CHALLENGE_CATEGORY}-${CHALLENGE_NAME}

docker-build:
	# build the container that compiles that can build the challenge
	docker build --build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} -t ${CI_REGISTRY_IMAGE}/${BUILD_IMAGE_TAG} -f Dockerfile.builder .
	# run the builder container and build the challenge
	docker run -v $(REPO_DIR):$(REPO_DIR) --rm ${CI_REGISTRY_IMAGE}/${BUILD_IMAGE_TAG} make build -C $(shell pwd)
	# build the container that hosts the challenge
	docker build --build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} -t $(CI_REGISTRY_IMAGE)/$(CHALLENGE_IMAGE_TAG) -f Dockerfile.challenge .
	# build the container that solves the challenge
	docker build --build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} -t $(CI_REGISTRY_IMAGE)/$(HEALTHCHECK_IMAGE_TAG) -f Dockerfile.healthcheck .

docker-test: docker-clean docker-build
	# run the challenge in the background
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} -d --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	# run the healthcheck to test against the challenge
	docker run --rm --link ${CHALLENGE_HOST} --name ${HEALTHCHECK_IMAGE_TAG} -e CHALLENGE_HOST=${CHALLENGE_HOST} -e CHALLENGE_PORT=${CHALLENGE_PORT} ${CI_REGISTRY_IMAGE}/$(HEALTHCHECK_IMAGE_TAG)
	# Now we're done delete the bits we created
	docker rm -f ${CHALLENGE_HOST} || true

docker-run: docker-clean docker-build
	# Run the challenge in the foreground
	docker run -t -i --expose ${CHALLENGE_PORT} --hostname ${CHALLENGE_HOST} --name ${CHALLENGE_HOST} --rm ${CI_REGISTRY_IMAGE}/${CHALLENGE_IMAGE_TAG}
	docker rm -f ${CHALLENGE_HOST} || true

docker-clean: clean
	docker rm -f ${CHALLENGE_HOST} || true
	docker rm -f ${HEALTHCHECK_IMAGE_TAG} || true
	docker network rm ${CHALLENGE_NETWORK} || true

clean:
	rm -rf build/ dist/ handout/
