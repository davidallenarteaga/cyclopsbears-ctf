# This is our container for compiling the challenge
# This should build everything we need for hosting or distributing
# the challenge, so that includes binaries, assets, and handouts

# This environment variable will contain the path to our
# container registry. It defaults to our private container registry
ARG CI_REGISTRY_IMAGE=registry.gitlab.com/cybears/fall-of-cybeartron

FROM gcc

# Add commands here to install any dependencies you need to
# compile or build your stuff.
