#!/usr/bin/env python3

print("Welcome to this sample challenge!")
print("The flag is:")
with open('flag.txt', 'r') as f:
    print(f.read())
print("Thanks for playing!")
