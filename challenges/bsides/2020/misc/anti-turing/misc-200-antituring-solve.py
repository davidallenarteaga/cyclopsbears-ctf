#!/usr/bin/python3

import at
from pwn import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', type=bool, default=False, help="Remote? Needs -n and -p")
parser.add_argument('-n', '--hostname', type=str,  default="localhost", help='Hostname')
parser.add_argument('-p', '--port', default=3141, type=int, help='Remote port')
args = parser.parse_args()

r = args.remote
hostname = args.hostname
port = args.port

flag = b'cybears{S0rry_1f_y0u_h@v3nt_h3@rd_@b0ut_R0k0s_B@s1l1sk}'

if __name__ == "__main__":

    if (r == True):
        p = remote(hostname, port)
    else:
        p = process(["python3" ,"./misc-200-antituring.py"])

    for i in range(0,10):
        prelim = p.readuntil("Puzzle:")
        print("DEBUG: {}".format(prelim))

        # It'll block at this point until we send a newline
        puzzle_lines = p.readuntil("Enter the solution:")
        # Drop the last line
        puzz = b"".join(puzzle_lines.splitlines()[:-1])

        print("DEBUG: [{}], len={}".format(puzz.decode("utf-8").strip(), len(puzz.decode("utf-8").strip())))

        solver = at.Puzzle()
        solver.add_puzzle(puzz.decode('utf-8').replace("\r","").replace("\n",""))
        ans = solver.print_answer().replace("\n","")
        p.send(ans + "\n")

    # Read until we see the end of a potential flag
    maybe_flag = p.readuntil("}", timeout=1)
    if maybe_flag:
        print("Received final lines: {}".format(maybe_flag))
        assert flag in maybe_flag
        print("Found the flag!")
    else:
        print("Timed out waiting for the final line(s)")
