sounds-of-godiego
=================

## Notes - Walkthrough
* Midi steg challenge - uses an adjusted sample of the monkey magic theme
* The flag is encoded into the midi using a combination of delta time values and program changes, although there is no additional complexity

* Players need to:
* - Identify the R29kaWVnbw==.mid (base64 for 'godeigo') file on the website
* - Use a combination of manual methods, or a tool such as stegano to find the flag

* Godeigo is the Japanese Rock band who authored the Monkey theme https://en.wikipedia.org/wiki/Godiego


# Reveal a secret
def reveal_message(midi_file, forget):
    print ("Reveal message in " + midi_file)

    # Read midi file
    tracks = midi.read_midifile(midi_file)

    # Define a map and an index to store secrets
    secrets = {}
    secret_index = 0
    forget_indexes = list()

    # Iterate track events
    for event in tracks[0]:
        i = 0

        # When find on event with specific characteristics, create a list in the map
        if isinstance(event, midi.NoteOnEvent) and event.tick == 0 and event.get_pitch() == midi.G_3:
            secrets[secret_index] = list()
            forget_indexes.append(i)

        # If find program change event store the character for the word into the map
        if isinstance(event, midi.ProgramChangeEvent) and event.tick == 0:
            secrets[secret_index].append(event.data[0])
            forget_indexes.append(i)

        # When find off event with specific characteristics, increment the index to store the next secret
        if isinstance(event, midi.NoteOffEvent) and event.tick == 0 and event.get_pitch() == midi.G_3:
            secret_index += 1
            forget_indexes.append(i)

        # Increment list index
        i += 1

    # Iterate the secrets collected
    for i in secrets:
        secret = ''

        # Join characters
        for char in reversed(secrets[i]):
            secret = secret + chr(char)

        # Print the secret
        print (secret)
