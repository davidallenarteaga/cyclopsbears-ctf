#!/bin/bash
set -o nounset
set -o pipefail
set -o errexit

SCRIPT_PATH="$(realpath -e "${BASH_SOURCE[0]}")"
SCRIPT_DIR="${SCRIPT_PATH%/*}"

STEGANO_MIDI="${SCRIPT_DIR}/stegano_midi/stegano-midi.py"
MIDI_FILE="${SCRIPT_DIR}/R29kaWVnbw==.mid"

if python "${STEGANO_MIDI}"  --reveal --file "${MIDI_FILE}" | grep 'cybears{'
then
    echo "SUCCESS! FLAG FOUND"
    exit 0
else
    echo "COULDN'T FIND FLAG"
    exit -1
fi
