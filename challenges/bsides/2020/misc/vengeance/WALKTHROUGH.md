# Vengeance

* This puzzle is an extension of the Jugs puzzle from Die Hard 3: Die Hard with a Vengeance
* In it, Bruce Willis' character needs to measure out exactly 4 gallons of water, given only a 3 gallon and a 5 gallon jug.
* https://mindyourdecisions.com/blog/2013/02/04/the-water-jug-riddle/
* In this puzzle, you are randomly given three buckets of memory which you can fill or move. You can also view the last filled cell of the buckets - the flag is contained within cell 1 of the smallest bucket.
* Given the construct of the problem, this is always solveable.
* There's a solver script in `solve.py`.

## References

