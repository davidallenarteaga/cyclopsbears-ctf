# SIGIL

Sigil is an algorithm challenge.

The player is presented with an asciiart that uses
the same algorithm as the randomart in openSSH and
a prompt.

They enter binary data in b64 and get back an asciiart
made from that data. This allows them to discover how
the algorithm works.

They should then try to draw the same asciiart by sending
inputs. If they send an input that generates the same ascii
art, we'll give them a flag.

To discover this they can reimplement the algorithm and hill
climb their way up.

The output is deliberately slow to avoid them brute forcing
against the server, they should discover the algorithm and
then implement it locally.
