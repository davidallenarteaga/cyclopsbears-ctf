# Optimal Prime

* _author_: Cybears:cipher
* _title_: Optimal Prime
* _points_: 400
* _tags_:  crypto, re

## Flags
* `cybears{Knock Knock, whos there? Lattice. Lattice who? Lattice in its cold outside!}`

## Challenge Text
```markdown
We've managed to intercept some encrypted messages and some kind of binary related to them. Can you help?
```

## Attachments
* `out.json`: json string with encrypted messages
* `optimalprime`: compiled binary that contains prime generation and RSA encryption

## Hints
* 

## References
* Coppersmith factoring with known bits https://eprint.iacr.org/2007/374.pdf

## Notes - Build
* `make` : to build handouts
* `make test` : to run solver and validate
* `make clean` : to remove artifacts
