# Flag facts! https://www.kickassfacts.com/25-interesting-facts-about-flags/
flag = "cybears{TheVirginiaStateFlagIsTheOnlyUSStateFlagToFeatureNudity}"

cool_facts = [
    #Transformers
    "COOL FACT: Orson Welles voices Unicron in the 1986 Transformers movie",
    "COOL FACT: Leonard Nemoy (Spock) voices Galvatron in the 1986 Transformers movie",
    "COOL FACT: The original name for the planet sized transformer called Unicron was Ingestor",
    "COOL FACT: The 1986 Transformers movie takes place in the then-distant year 2005",
    "COOL FACT: It was illegal to import the original Megatron toy into Australia due to it's resemblance to a Walther P38 handgun",
    "COOL FACT: Marvel Comics helped Hasbro come up with the names for Optimus Prime and Megatron",
    #Mathematics/Crypto
    "COOL FACT: The average number of ways to express an integer as the sum of two squares is pi",
    "COOL FACT: There is a way of estimating pi using dropping a pin onto a flat surface. It's called Buffon's Needle",
    "COOL FACT: The symbol for infinity was used by the Romans to represent 1000",
    "COOL FACT: If you concatenate all the palindromes from 1 to 101, the number produced would be prime!",
    "COOL FACT: From 0 to 1000, only the number one thousand has the letter A",
    "COOL FACT: 4 is the only number with the same number of letters in English",
    "COOL FACT: The Reuleaux Triangle is a shape of constant width other than a circle",
    "COOL FACT: 40 when written -forty- is the only number with letters in alphabetical order",
    #Hackers/Computer Security
    "COOL FACT: William Gibson invented the term -Cyberspace- in 1982 for his book, Neuromancer."
]
