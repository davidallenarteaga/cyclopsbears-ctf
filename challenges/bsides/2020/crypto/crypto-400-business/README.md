# Ordinary Course of Business

* _author_: Cybears:cipher
* _title_: Ordinary Course of Business
* _points_: 400
* _tags_:  crypto

## Attachments
* `handout/business`: Server binary

## Notes - Build server
* `gcc ocb.c rijndael-alg-fst.c ordinaryCourseOfBusiness.c -o out && ./out`
* `gcc ocb.c rijndael-alg-fst.c ordinaryCourseOfBusiness.c -o out -DDEBUG -DADDAD`
* run locally:
* `socat -d TCP-LISTEN:3141,reuseaddr,fork EXEC:"./business"`
* UPDATED: `make -B all`

## Notes - Build attack
* `gcc -Wall -c -fPIC ocb.c rijndael-alg-fst.c OCB2Attack.c`
* `gcc -shared -o ocb_attack_shared.so OCB2Attack.o ocb.o rijndael-alg-fst.o`
* UPDATED: `make -B all`

## Notes - solver
* `make test`

## References
* OCB2 code: https://web.cs.ucdavis.edu/~rogaway/ocb/code-2.0.htm

