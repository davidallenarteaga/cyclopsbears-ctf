# Bomb Disposal walkthrough

This is a standalone challenge where players need to recover the internal state of a custom RSA based Random Number Generator (RNG). Players are provided the source code for the RNG, as well as encrypted output. 

The basis of this attack is the Franklin-Reiter related message attack.

## Hints
1. Hint
<details>
It's not actually a BBS RNG...
</details>

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

* The Franklin-Reiter related message attack on RSA works on stereotyped RSA messages. The wikipedia article, and almost all other references discuss the relationship between two messages M1 and M2 as a linear/affine function f(M2) = A*M1+B, but, in reality,  and in the original paper, the relationship can be a polynomial of any degree. 
* In this challenge, the polynomial relationship is a cubic and the public exponent is 2. 
* The solve script (written in sage), makes a custom polynomial-ring GCD function, and then recovers the internal state of the RNG based on seeing two consecutive external states (aka encrypted messages). 

</details>

## Steps - Playtester alternative solution to original challenge

<details>
<summary>Spoiler warning</summary>

The original challenge had the internal stepping function as a quadratic, not a cubic, but the playtester found a neat algebraic bypass as follows:

* Consider the internal states as `s_i` and the external states as `x_i`
* Then, `s_{i+1} = B*s_i^2 + C*s_i + D` and `x_i = s_i^2` (all operations modulo N)
* We can write `y_i = B*x_i + D = s_{i+1} - C*s_i`
* Now we have two equations: 
* (1) `s_{i+1} + C*s_i = (x_{i+1}-c^2*x_i)/y_i`
* (2) `s_{i+1}-C*s_i = y_i`
* The right-hand side and C are all known, solve for `s_0` and `s_1`!

</details>


## References

- [Wikipedia](https://en.wikipedia.org/wiki/Coppersmith%27s_attack#Franklin-Reiter_related-message_attack)
- [Original paper](https://link.springer.com/content/pdf/10.1007/3-540-68339-9_1.pdf)
