# Despicable Key

* _author_: Cybears:cipher
* _title_: Despicable Key
* _points_: 200
* _tags_:  forensics,crypto

## Flags
* `cybears{BeAnOptimistPrimeNotANegatron!}`

## Challenge Text
```markdown
Help! I've securely encrypted my file. It's OK, I remember the key, but
not the IV or the tag... can you help?
```

## Attachments
* `enc.py`: encryption program (python3)
* `flag.png.enc`: Flag encrypted with enc.py

## Hints
 * -

## References
* https://www.reddit.com/r/crypto/comments/b2psg5/decrypt_aes256gcm_if_key_and_ciphertext_are_known/
* https://en.wikipedia.org/wiki/Galois/Counter_Mode
* https://en.wikipedia.org/wiki/Portable_Network_Graphics#File_format

## Notes - Build
* `make all` : to build handouts
* `make clean` : to remove artifacts
* Most of the time pycryptodome is a drop-in replacement for PyCrypto (both use import Crypto namespace). For a couple of reasons, i need both (Pycryptodome has AESGCM, but pycrypto has working AES CTR), so I install pycryptodomex, which gives it the `Cryptodome` namespace.
