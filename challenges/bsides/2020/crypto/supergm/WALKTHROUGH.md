# supergm walkthrough

* In this challenge, players are handed a file which contains the some helpful advice and some polynomials x,N and ciphers.
* The helpful advice, when read down the first column, reads POLYGOLDWASSERMICALI.
* Goldwasser-Micali is an elegant bit encryption scheme based on the hardness of knowing whether an element in a ring Z/(pq)Z is a square.
* In this scheme, instead of using 512+ bit prime numbers to be p,q we instead work in the universe F_3[X]/<pq> where p,q are degree 255-ish irreducible polynomials in F_3[x]
* Since it's easy to factorise polynomials, once can fairly simply detect squares in F_3[X]/<N(X)>
* The algorithmic idea is:
*   factor N(X) = p(X)q(X)
*   take a cipher polynomial C(X) and compute CP(X) = C(X) mod p(X) and CQ(X) = C(X) mod q(X)
*   As in Goldwasser-Micali check if CP(X) is a square mod P(X). This can be done in the finite field isomorphic to F_3[X] / <P(X)> or directly with the polynomial representation.
*   I personally used a built-in IsSquare function in magma/sage but...
*   One of the playtesters computed (CP(X) mod P(X)) ^ ((3^255-1)/2) mod P(X) and checked if it was equal to 1. To see why that works, think about C(P) mod P(X) as g^e for some generator g and power e and the parity of e can be deduced by raising g^e to the correct power.
*   Square means the corresponding plaintext is 0, non-square means the corresponding plaintext is 1.
