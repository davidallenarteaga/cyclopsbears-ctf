#!/bin/bash

#kill all with "pkill socat"
socat TCP-LISTEN:9000,reuseaddr,fork EXEC:"python3 ./server.py" &
socat TCP-LISTEN:9001,reuseaddr,fork EXEC:"python3 ./client_responder.py" & #9001 Flag Cybears
socat TCP-LISTEN:9002,reuseaddr,fork EXEC:"python3 ./client_responder.py -i 9002" & #9002 Dec
socat TCP-LISTEN:9003,reuseaddr,fork EXEC:"python3 ./client_responder.py -i 9003" & #9003 Cybears
socat TCP-LISTEN:9004,reuseaddr,fork EXEC:"python3 ./client_responder.py -i 9004" & #9004 Dec
socat TCP-LISTEN:9005,reuseaddr,fork EXEC:"python3 ./client_responder.py -i 9005" & #9005 Cybears
socat TCP-LISTEN:9007,reuseaddr,fork EXEC:"python3 ./client_responder.py -i 9007" & #9007 None
