## Cybears CTF
## Rot Away Rust - Secrets file

flag=b'cybears{Typ3s_0f_C0nfu510n}'
# dictionary of clients, indexed by client_id
# client-server keys were randomly generated using the following. Need to be hardcoded for consistency. Only guest key will be revealed to players
#for i in range(0,6):
#    print("    {} : {{ 'name' : 'name{}', 'affiliation' : 'cybears',  'server_key' : '{}' }},".format(hex(0x10000+i), i, hexlify(os.urandom(16))))

secrets_server = {
    9001 : { 'name' : 'Flagimus Prime', 'affiliation' : 'cybears',  'server_key' : 'd41b800964e215c55e2b86d5e8b14ff4', 'port' : 9001 },
    9002 : { 'name' : 'Pandamonium', 'affiliation' : 'decepticomtss',  'server_key' : 'fce78e485dfef9b9d733c1cf4b1572e5', 'port' : 9002 },
    9003 : { 'name' : 'Ursine Magnus', 'affiliation' : 'cybears',  'server_key' : '88a86609b83c38e80fe5457c811f7c2e', 'port' : 9003 },
    9004 : { 'name' : 'Barbearian', 'affiliation' : 'decepticomtss',  'server_key' : 'f8236c2cac46dd997a3d15071b0d9fe7', 'port' : 9004 },
    9005 : { 'name' : 'Bumblebear', 'affiliation' : 'cybears',  'server_key' : '9eb6a39b57dc7a9ccc03162efbd2af10', 'port' : 9005 },
    9006 : { 'name' : 'guest', 'affiliation' : 'none',  'server_key' : 'd7e0a7a24c1472c1e4b203a7c235f6a4', 'port' : 9006 },
    9007 : { 'name' : 'echo', 'affiliation' : 'none',  'server_key' : '49b3acef9f97df0c2fa89e6e9231bc66', 'port' : 9007 }
}
