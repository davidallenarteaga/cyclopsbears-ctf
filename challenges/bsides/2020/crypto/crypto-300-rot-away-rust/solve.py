import json
import Crypto.Cipher.AES as aes
import Crypto.Random as rand
from binascii import *
import struct
from pwn import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--clientid', type=int, default=9003, help="client_id of client_initiator")
parser.add_argument('-s', '--serveraddress',  default="localhost", help="server address (domain or IP string)")
parser.add_argument('-t', '--responderaddress',  default="localhost", help="client responder address (domain or IP string)")
parser.add_argument('-p', '--serverport', type=int, default=9000, help="Port number of server")
parser.add_argument('-q', '--responderport', type=int, default=9001, help="Port number of client responder")
parser.add_argument('-d', '--debug', help="Print debug", action="store_true")
args = parser.parse_args()

if args.debug:
    context.log_level = 'debug'
else:
    context.log_level = 'info'

server_address = args.serveraddress
server_port = args.serverport
responder_address = args.responderaddress
responder_port = args.responderport

log.debug("s_add - {}\ns_port - {}\ncr_add - {}\ncr_port - {}".format(server_address, server_port, responder_address, responder_port))

client_id = args.clientid

####Initiator
log.info("Connecting to Client Responder...")
try:
    cr = remote(responder_address, responder_port)
except Exception as e:
    log.error("ERROR: Could not connect to client responder: ({},{}) : {}".format(responder_address, responder_port, e))
    exit(-1)

log.info("STEP 1 - send client_id to client_responder")
## Step 1 - send client_id to client_responder
cr.sendline(json.dumps({"ID_A" : client_id}))

## Step 2 - receive from client_responder
log.info("STEP 2 - receive from client_responder")
step2r = cr.recvuntil(b'}')
log.debug(step2r)

try:
    step2rj = json.loads(step2r)
except Exception as e:
    print("ERROR: Expecting JSON with key success, session_id, ID_A, ID_B, gcm_nonce, gcm_cipher, gcm_tag")
    exit(-1)

step2r_keys = {'success', 'session_id', 'ID_A', 'ID_B', 'gcm_nonce', 'gcm_cipher', 'gcm_tag'}

if 'success' in step2rj.keys():
    step2r_success = step2rj['success']
else:
    print("ERROR: Expecting JSON with key success")
    exit(-1)

if step2r_success != True:
    print("ERROR, step2r failed: {}".format(step2rj['error']))
    exit(-1)

## Step 5 solve - replay initial response to client_responder
## There is a type confusion by the client responder - based on the fact that K_BS and session_id||ID_A||ID_B are the same length
log.info("STEP 5 solve - replay initial response to client_responder")

step5s = {  'session_id' : step2rj['session_id'],
            'gcm_nonce' : step2rj['gcm_nonce'], #Just pass on encrypted details from B to... B!
            'gcm_cipher' :  step2rj['gcm_cipher'],
            'gcm_tag':  step2rj['gcm_tag']
        }

cr.sendline(json.dumps(step5s))

## Step 6 - receive message from client_responder
log.info("STEP 6 - receive message from client_responder ")
step6r = cr.recvuntil(b'}')

log.debug(step6r)

step6rj = json.loads(step6r)

derived_key = unhexlify(step2rj['session_id']) + struct.pack("I", client_id) + struct.pack("I", step2rj['ID_B'])

try:
    e6 = aes.new(derived_key, mode=aes.MODE_GCM, nonce=unhexlify(step6rj['gcm_nonce']))
    log.success("MESSAGE RECEIVED! {}".format(e6.decrypt_and_verify(unhexlify(step6rj['gcm_cipher']), unhexlify(step6rj['gcm_tag']))))
except Exception as e:
    print("ERROR in step 6 decryption: {}".format(e))
    cr.close()
    exit(-1)

#######################################################
cr.close()
exit(0)
#######################################################
