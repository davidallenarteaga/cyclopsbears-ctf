# SSSH Walkthrough

* This challenge utilises Shamir's Secret Sharing Scheme to split a flag into 4-pieces, such that players need all 4 (four) pieces to reconstruct.
* The shares are embedded in QR-codes and these will be stuck up around BSides conference venue for players to find. Some should be rare/hard to find, others should be easier, and some will have duplicates printed (the index of the share is in the QR code).
* We'll need to make sure that players don't tear down the images to stop other players finding the flags (print extra copies of all shares)
* To solve using pycryptodome after finding the QR codes and putting the shares into the "shares" array:
`from Crypto.Protocol.SecretSharing import Shamir
Shamir.combine(shares)`
* To solve using the ssss tool (http://point-at-infinity.org/ssss/), you use it with 128 bit security (parameter “-s 128”) and no dispersion (parameter “-D”).

## Locations
* 1: behind cybears banner at door
* 2: main lectern in conference hall
* 3: embedded in webpage front page - hidden div with id=ssssh
* 4: back of cybears.ciphers laptop
* 5: girls toilets
* 6: boys toilets
* 7: registration desk
* 8: embedded in badge??
* url: lots, scattered around everywhere
* hint: lots, scattered around everywhere
* rickroll: lots, scattered around everywhere
