# Fun With Primes

* _author_: Cybears:cipher
* _title_: Fun With Primes
* _points_: 50
* _tags_:  crypto, beginner


## Challenge Text
```markdown
Primes and number theory are a critical part of cryptography. Try this series of short puzzles to get a flag!

        `nc $target_dns $target_port`
```



## Notes - Build
* run and solve locally:
* `socat -d TCP-LISTEN:3141,reuseaddr,fork EXEC:"python3 ./FunWithPrimes_server.py"`
* `python3 FunWithPrimes_solve.py -r localhost:3141`

* run and test with docker
* `docker network create --driver bridge cybearsnet`
* `docker build -t fwp -f Dockerfile.server . `
* `docker build -t fwp_test -f Dockerfile.solve .`
* `docker run -it -p 3141:3141 --network=cybearsnet --name fwp_server fwp `
* `docker run -it --rm -e HOST=fwp_server -e PORT=3141 --network=cybearsnet --name fwp_tester fwp_test`
