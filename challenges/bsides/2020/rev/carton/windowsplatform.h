/**
 * Carton: Windows platform-specific functions
 **/
#pragma once
#include "platform.h"
#include <cstdarg>
#include <cstdio>
#include <Windows.h>



class WindowsPlatform : public GamePlatform
{
    public:

        virtual void Exit()
        {
            ExitProcess(0);
        }

        virtual void FlashLED(LedColor t1, LedColor t2, LedColor t3, LedColor t4)
        {
            OutputDebugStringA("FlashLED not implemented");
        }

        virtual void DebugPrint(const char * message, ...)
        {
            char temp_buffer[256] = {0};
            va_list args;
            va_start(args, message);
            vsprintf(temp_buffer, message, args);
            va_end(args);
            OutputDebugStringA(temp_buffer);
        }

};