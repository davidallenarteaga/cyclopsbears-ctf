/**
 * Carton: Game states
 **/
#pragma once

#include <cstdint>
#include <cstring>
#include <SDL.h>
#include "platform.h"
#ifdef XBOX
#include "xboxplatform.h"
#else
#include "windowsplatform.h"
#endif

class CGame;

// Abstract game state
class CGameState {
public:
    CGameState(CGame& game)
        : game(game)
    {}

    virtual ~CGameState()
    { }

    // Handle keyboard input.
    virtual void HandleInput(SDL_Keycode keycode) = 0;

    // Update state. Return a new state if requesting to transition to another state.
    virtual CGameState * Update() = 0;

    // Render this state to the display.
    virtual void Render() = 0;
protected:
    CGame & game;
};

// Utilities
class CFont {
public:
    CFont(SDL_Renderer * renderer, SDL_Texture * font_texture, const uint32_t cell_width, const uint32_t cell_height);

    // Calculate the width and height of a rendered string
    void Measure(const char * text, uint32_t * width, uint32_t * height);

    // Render a string of text
    void Render(const char * text, const uint32_t x, const uint32_t y);

    void SetBorder(const uint32_t border_x, const uint32_t border_y);
private:

    SDL_Renderer * Renderer;
    SDL_Texture * Texture;
    uint32_t CellWidth, CellHeight;
    uint32_t TextureWidth, TextureHeight;
    uint32_t BorderX, BorderY;
};

// Create a font from a bitmap file on disk
CFont * LoadFont(SDL_Renderer * renderer, const char * filename, const uint32_t cell_width, const uint32_t cell_height);


class CGame {
public:

    CGame(GamePlatform& platform)
        : Platform(platform)
    { }

    ~CGame();

    // Set up everything
    bool Initialize(SDL_Renderer * renderer, CGameState * initial_state);

    // Start the game
    void MainLoop();

    // Request that the main loop be terminated
    void RequestExit()
    {
        ExitRequested = true;
    }

    // Request that the frame is re-rendered
    void RequestRepaint()
    {
        IsDirty = true;
    }

    CFont& GetDefaultFont()
    {
        return *(this->DefaultFont);
    }

    SDL_Renderer * GetRenderer()
    {
        return this->Renderer;
    }

    GamePlatform& GetPlatform()
    {
        return Platform;
    }

private:
    CGameState * State;
    CFont * DefaultFont = nullptr;

    SDL_Renderer * Renderer;
    bool IsDirty = true;

    bool ExitRequested = false;

    GamePlatform& Platform;
};


// Game states

// Enter password: prompts the player for the password
class CEnterPasswordState : public CGameState {
public:

    CEnterPasswordState(CGame& game)
        : CGameState(game)
    {
        memset(password, 'A', kPasswordLength);
    }

    virtual void HandleInput(SDL_Keycode keycode);
    virtual CGameState * Update();
    virtual void Render();

private:
    const uint32_t kPasswordLength = 12;

    char password[32] = { 0 };
    uint8_t pos = 0;
    bool entered_password = false;
};

// Bad password state: shows a "bad password" message.
class CBadPasswordState : public CGameState {

public:
    CBadPasswordState(CGame& game)
        : CGameState(game)
    {}

    virtual void HandleInput(SDL_Keycode keycode);
    virtual CGameState * Update();
    virtual void Render();
private:
    bool retry_requested = false;
};

// Good password state: decrypts the flag and displays it.
class CGoodPasswordState : public CGameState {

public:
    CGoodPasswordState(CGame& game, const char * entered_password)
        : CGameState(game)
    {
        password = strdup(entered_password);
    }


    ~CGoodPasswordState()
    {
        free(password);
    }

    virtual void HandleInput(SDL_Keycode keycode);
    virtual CGameState * Update();
    virtual void Render();
private:
    uint32_t frames = 0;
    char * password = nullptr;
    char decrypted_flag_message[64];
};

// Checking password state: displays an animation then checks if the password is correct.
class CCheckingPasswordState : public CGameState {

public:
    CCheckingPasswordState(CGame& game, const char * entered_password)
        : CGameState(game)
    {
        password = strdup(entered_password);
    }

    ~CCheckingPasswordState()
    {
        free(password);
    }

    virtual void HandleInput(SDL_Keycode keycode);
    virtual CGameState * Update();
    virtual void Render();
private:
    uint32_t frames = 0;
    char * password = nullptr;
};


// "Anti-cheat"
void DoAntiCheat(GamePlatform& platform);
