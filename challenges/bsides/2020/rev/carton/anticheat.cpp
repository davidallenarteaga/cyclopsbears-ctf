/**
 * Carton: "anti-cheat" game file integrity check
 **/
#include "game.h"
#include <cstdio>
#include <cctype>
#include <windows.h>

extern "C" {
#include <WjCryptLib_Md5.h>
}

// List of files to check the hashes of
const char * g_GameFiles[] = {
    "D:\\default.xbe",
    "D:\\barcade.bmp"
};
const uint32_t g_NumberOfGameFiles = 2;


// Convert a single hex digit into a char
char hex_digit_to_char(char h)
{
    if (h >= '0' && h <= '9')
    {
        return h - '0';
    }
    else if (h >= 'a' && h <= 'f')
    {
        return h - 'a' + 10;
    }
    else if (h >= 'A' && h <= 'F')
    {
        return h - 'A' + 10;
    }
    return 0;
}

// Calculate the MD5 hash of a file
void HashFile(const char * filename, MD5_HASH * hash)
{
    // Initialize the hash context
    Md5Context md5ctx;
    Md5Initialise(&md5ctx);

    char temp_buffer[512] = { 0 };
    FILE * this_file = fopen(filename, "rb");
    if (this_file)
    {
        while (!feof(this_file))
        {
            size_t chunk_size = fread(temp_buffer, 1, sizeof(temp_buffer), this_file);
            if (chunk_size > 0)
            {
                Md5Update(&md5ctx, temp_buffer, chunk_size);
            }
            else
            {
                // at the end of the file
                break;
            }
        }
        fclose(this_file);
    }

    Md5Finalise(&md5ctx, hash);
}

// Load the hash file, check each game file against the hash list, and error out if any of them don't match.
void DoAntiCheat(GamePlatform& platform)
{
    bool bad = false;
    char line_buffer[64] = { 0 };

    // Allocate a buffer to hold the hash list
    const uint32_t number_of_hashes = 16;
    MD5_HASH * md5_hashes = (MD5_HASH*)VirtualAlloc(0, number_of_hashes * sizeof(MD5_HASH), MEM_COMMIT, PAGE_READWRITE);
    memset(md5_hashes, 0, number_of_hashes * sizeof(MD5_HASH));

    // Load the hash list from disk
    FILE * hash_file = fopen("D:\\anticheat.txt", "r");
    if (hash_file)
    {
        uint32_t current_hash = 0;

        // Read each hash
        while (fgets(line_buffer, sizeof(line_buffer) - 1, hash_file))
        {
            char * line_pos = line_buffer;

            // ignore whitespace
            while (*line_pos != 0 && !isxdigit(*line_pos))
                line_pos++;

            if (strlen(line_pos) < 32)
            {
                continue;
            }

            for (uint32_t i = 0; i < 32; i += 2)
            {
                md5_hashes[current_hash].bytes[i / 2] = (hex_digit_to_char(line_pos[i]) << 4) | (hex_digit_to_char(line_pos[i + 1]));
            }
            current_hash++;

            if (current_hash >= number_of_hashes)
            {
                break;
            }
        }

        fclose(hash_file);
    }
    else
    {
        platform.DebugPrint("\nFailed to open anticheat hash file!");
        bad = true;
    }

    // Hash the game files
    for (int i = 0; i < g_NumberOfGameFiles && !bad; i++)
    {
        MD5_HASH this_file_hash = { 0 };
        HashFile(g_GameFiles[i], &this_file_hash);

        bool found_file = false;
        for (int known_hash = 0; known_hash < number_of_hashes && !found_file; known_hash++)
        {
            if (memcmp(this_file_hash.bytes, md5_hashes[known_hash].bytes, 16) == 0)
            {
                // found it
                found_file = true;
            }
        }

        if (!found_file)
        {
            platform.DebugPrint("\nHash of file %s not found in known-good list!", g_GameFiles[i]);
            bad = true;
        }
    }


    // Check if the binary has been modified
    if (bad)
    {
        // Flash the light red
        platform.FlashLED(LedColor::kRed, LedColor::kOff, LedColor::kOff, LedColor::kRed);
        platform.DebugPrint("\nCheating detected! Disabling!");
        ::Sleep(5000);
        platform.Exit();
    }

    // NOTE: we intentionally leak the allocation containing the hashes.
    // Some of the hashes are actually bits of executable code that are found by the egg hunter.
}