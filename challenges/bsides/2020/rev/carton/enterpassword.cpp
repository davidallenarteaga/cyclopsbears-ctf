/**
 * Carton: Password entry state
 **/

#include "game.h"
#include <cstdio>
#include <string>


void CEnterPasswordState::HandleInput(SDL_Keycode keycode)
{
    switch (keycode)
    {
    case SDLK_LEFT:
    case SDLK_BACKSPACE:
        if (pos > 0)
            pos--;
        break;
    case SDLK_RIGHT:
        if (pos < (kPasswordLength - 1))
            pos++;
        break;
    case SDLK_UP:
        if (password[pos] < 'Z')
        {
            password[pos]++;
        }
        break;
    case SDLK_DOWN:
        if (password[pos] > 'A')
        {
            password[pos]--;
        }
        break;
    case SDLK_RETURN:
        entered_password = true;
        break;
    case SDLK_ESCAPE:
        game.RequestExit();
        break;
    default:
    {
        // Handle keyboard input
        if (keycode >= SDLK_a && keycode <= SDLK_z)
        {
            password[pos] = (keycode - SDLK_a) + 'A';
            if (pos < (kPasswordLength - 1))
            {
                pos++;
            }
        }

    }
    break;
    }

    game.RequestRepaint();
}

CGameState * CEnterPasswordState::Update()
{

    if (entered_password)
    {
        return new CCheckingPasswordState(this->game, password);
    }
    else
    {
        return nullptr;
    }
}

void CEnterPasswordState::Render()
{
    char enter_password_message[64];
    sprintf(enter_password_message, "Enter  Password:\n  %s", password);

    // TODO: Center text?

    this->game.GetDefaultFont().Render(enter_password_message, 64, 64);

    // Render frame count
}