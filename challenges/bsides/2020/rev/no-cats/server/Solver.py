import socket
import argparse
import json
import config

parser = argparse.ArgumentParser()
parser.add_argument("--target_dns", type=str, help='The server address.', default='no-cats.ctf.cybears.io')
parser.add_argument("--target_port", type=int, help='The server port.', default=9001)
args = parser.parse_args()

conn = socket.create_connection((args.target_dns, args.target_port), timeout=10)
conn.send(b'GET\x00ENL\x00IGH\x00TENED')

data = conn.recv(2048)
print(data.decode())
if json.loads(data.decode('utf-8').rstrip())['0'] == config.FLAG:
    exit(0)
else:
    exit(1)
