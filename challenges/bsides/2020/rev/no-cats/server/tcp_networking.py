from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import Factory
import config, serverdata
import json

# todo listen for flag requests on a different port/server? to avoid congestion.

class EnlightenmentFactory(Factory):
    def buildProtocol(self, addr):
        return EnlightenmentProtocol(self)


class EnlightenmentProtocol(Protocol):

    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):
        print(f'got connection from {self.transport.getPeer()}.')


    def dataReceived(self, data: bytes):

        if data == bytes([0x47,0x45, 0x54, 0x00, 0x45, 0x4e, 0x4c, 0x00, 0x49, 0x47, 0x48, 0x00, 0x54, 0x45, 0x4e, 0x45, 0x44]):
            print(f'{self.transport.getPeer()} requested flag.')
            self.transport.write(bytes(json.dumps({0: config.FLAG}), encoding='utf-8'))
            self.transport.loseConnection()

        try:
            # attempt to decode the data into a dictionary.
            raw_data = json.loads(data.decode('utf-8'))

            if "uid" in raw_data and "pos" in raw_data:
                assert isinstance(raw_data['uid'], str)
                assert isinstance(raw_data['pos'], int)
                # dictionary should be {uid: str, pos: int}
                # send the data to the ServerController to keep track of the player's position.
                if serverdata.ServerController.getInstance().handle_player_data(raw_data['uid'], raw_data['pos'], self.transport.getPeer()):
                    packet = serverdata.ServerController.getInstance().prepare_public_details(raw_data['uid'])
                    self.transport.write(packet)
                else:
                    self.transport.write(b"There are no cats allowed in the shrine!")
                    self.transport.loseConnection()
        except:
            pass

