import uuid
import datetime

class Player:

    __DOC__ = """
     Not used yet
     a container class to keep track of which private_uid is assigned to which address,
    can be used to also keep age data, if we want people to age off - and flairs like colour, nicknames.
    I included the privilege var there because we can also probably engineer that as part of the challenge.
    """

    def __init__(self, uid, addr, privilege=0, x=0):
        self.private_uid = uid
        self.public_uid = uuid.uuid4()
        self.addr = addr
        self.privilege = privilege
        self.x = 0
        self.last_contact = datetime.datetime.now().timestamp()
