import pygame
from spritesheet import Spritesheet
import datetime
import random
import math


class OtherPlayer(pygame.sprite.Sprite):
    def __init__(self, uid, pos):
        pygame.sprite.Sprite.__init__(self)
        self.uid = uid
        self.last_seen = datetime.datetime.now().timestamp()
        self.alpha = 190
        self.colour = tuple((random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
        self.FACING_LEFT = False, False, False
        self.destination = pos
        self.load_frames()
        self.rect = self.idle_frames_left[0].get_rect()
        self.rect.midbottom = (pos, 244)
        self.current_frame = 0
        self.last_updated = 0
        self.velocity = 0
        self.state = 'idle'
        self.current_image = self.idle_frames_left[0]
        self.shadow_image = None
        self.left_border, self.right_border = 250, 1150
        self.ground_y = 224
        self.box = pygame.Rect(self.rect.x, self.rect.y, self.rect.w * 2, self.rect.h)
        self.box.center = self.rect.center
        self.passed = False

    def draw(self, display):
        self.current_image.set_alpha(self.alpha)
        display.blit(self.current_image, self.rect)

    def update(self):
        self.move_to(self.destination)
        self.set_state()
        self.animate()
        self.shadow()

    def set_state(self):
        self.state = 'idle'
        if self.destination > self.rect.x:
            self.state = 'moving right'
        elif self.destination < self.rect.x:
            self.state = 'moving left'

    def move_to(self, x):
        if self.rect.x == x:
            return
        if x < self.rect.x:
            self.FACING_LEFT = True
            self.rect.x -=  8 if (x - self.rect.x) > 3 else 1
        else:
            self.FACING_LEFT = False
            self.rect.x += 8 if (self.rect.x - x) > 3 else 1



    def animate(self):
        now = pygame.time.get_ticks()
        if self.state == 'idle':
            if now - self.last_updated > 100:
                self.last_updated = now
                self.current_frame = (self.current_frame + 1) % len(self.idle_frames_right)
                if self.FACING_LEFT:
                    self.current_image = self.idle_frames_left[self.current_frame]
                elif not self.FACING_LEFT:
                    self.current_image = self.idle_frames_right[self.current_frame]
        else:
            if now - self.last_updated > 100:
                self.last_updated = now
                self.current_frame = (self.current_frame + 1) % len(self.walking_frames_right)
                if self.state == 'moving left':
                    self.current_image = self.walking_frames_left[self.current_frame]
                elif self.state == 'moving right':
                    self.current_image = self.walking_frames_right[self.current_frame]
        self.current_image.set_alpha(self.alpha)

    def shadow(self):
        self.shadow_image = self.current_image.copy()
        self.shadow_image.convert_alpha()
        self.shadow_image.fill(self.colour, None, pygame.BLEND_RGBA_SUB)
        self.shadow_image.set_alpha(self.alpha)

    def load_frames(self):
        my_spritesheet = Spritesheet('hero.png')
        #pygame.image.load('MY_IMAGE_NAME.png').convert()
        self.idle_frames_right = [my_spritesheet.parse_sprite("cat_idle_blink_strip8-0.png"),
                                 my_spritesheet.parse_sprite("cat_idle_blink_strip8-1.png"),
                                 my_spritesheet.parse_sprite("cat_idle_blink_strip8-2.png"),
                                 my_spritesheet.parse_sprite("cat_idle_blink_strip8-3.png"),
                                 my_spritesheet.parse_sprite("cat_idle_blink_strip8-4.png"),
                                 my_spritesheet.parse_sprite("cat_idle_blink_strip8-5.png"),
                                 my_spritesheet.parse_sprite("cat_idle_blink_strip8-6.png"),
                                 my_spritesheet.parse_sprite("cat_idle_blink_strip8-7.png"),
                                 my_spritesheet.parse_sprite("cat_idle_strip8-0.png"),
                                 my_spritesheet.parse_sprite("cat_idle_strip8-1.png"),
                                 my_spritesheet.parse_sprite("cat_idle_strip8-2.png"),
                                 my_spritesheet.parse_sprite("cat_idle_strip8-3.png"),
                                 my_spritesheet.parse_sprite("cat_idle_strip8-4.png"),
                                 my_spritesheet.parse_sprite("cat_idle_strip8-5.png"),
                                 my_spritesheet.parse_sprite("cat_idle_strip8-6.png"),
                                  my_spritesheet.parse_sprite("cat_idle_strip8-7.png")]
        self.walking_frames_right = [my_spritesheet.parse_sprite("cat_run_strip4-0.png"),
                                    my_spritesheet.parse_sprite("cat_run_strip4-1.png"),
                                    my_spritesheet.parse_sprite("cat_run_strip4-2.png"),
                                    my_spritesheet.parse_sprite("cat_run_strip4-3.png")
                                    ]
        self.idle_frames_left = []
        for frame in self.idle_frames_right:
            self.idle_frames_left.append( pygame.transform.flip(frame,True, False) )
        self.walking_frames_left = []
        for frame in self.walking_frames_right:
            self.walking_frames_left.append(pygame.transform.flip(frame, True, False))



