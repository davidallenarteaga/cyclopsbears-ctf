import Foundation
import Dispatch

public class Comms {
    var sock_fd: Int32
    let port = 8888
    
    let bound_fd: Int32
    
    let queue = DispatchQueue.init(label: "CommsQueue", qos: .utility, attributes: .concurrent, autoreleaseFrequency: .inherit, target: .global(qos: .utility))
    
    func dispatch(_ options: ModuleOptions) -> ModuleReturnData {
        #if DEBUG
        print("Received: \(options)")
        #endif
        switch options.type {
        case .Download:
            return downloadModule.run(options)
        case .ListDirectory:
            return listDirModule.run(options)
        case .comms:
            return ModuleReturnData.init(type: .comms, status: .BadType, data: nil)
        }
    }
    
    public init() {
        #if os(Linux)
        sock_fd = socket(PF_INET, Int32(SOCK_STREAM.rawValue), Int32(IPPROTO_TCP))
        #else
        sock_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)
        #endif
        guard sock_fd != -1 else {
            #if DEBUG
            print("Failed to create socket \(errno))")
            #endif
            abort()
        }
        
        var hints = addrinfo.init()
        #if os(Linux)
        hints.ai_socktype = Int32(SOCK_STREAM.rawValue)
        #else
        hints.ai_socktype = SOCK_STREAM
        #endif

        hints.ai_flags = AI_PASSIVE
        hints.ai_family = AF_INET
        
        var server_info: UnsafeMutablePointer<addrinfo>?
        if 0 != getaddrinfo(nil, "8888", &hints, &server_info) {
            #if DEBUG
            print("Failed to get info \(errno)")
            #endif
            close(sock_fd)
            abort()
        }
        
        bound_fd = bind(sock_fd, server_info!.pointee.ai_addr, socklen_t.init(server_info!.pointee.ai_addrlen))
        
        guard bound_fd >= 0 else {
            #if DEBUG
            print("Failed to bind \(errno)")
            #endif
            close(sock_fd)
            abort()
        }
        
        guard listen(sock_fd, 1) >= 0 else {
            #if DEBUG
            print("Failed to listen \(errno)")
            #endif
            close(sock_fd)
            abort()
        }
    }
    
    deinit {
        close(sock_fd)
    }
    
    func handleClient(_ fd: Int32) {
        #if DEBUG
        print("Connection open \(fd)")
        #endif
        defer {
            #if DEBUG
            print("Closing connection \(fd)")
            #endif
            close(fd)
        }
        let payloadBuffSize = 512
        let maxSize = 2048
        var payload: Data = .init()

        while true {
            if payload.count >= maxSize {
                #if DEBUG
                print("Max size reached: \(payload.count)")
                #endif
                return
            }
            
            let payloadBuff = UnsafeMutablePointer<UInt8>.allocate(capacity: payloadBuffSize)

            let size = read(fd, payloadBuff, payloadBuffSize)
            if size == 0 {
                break
            }
            guard size > 0 else {
                #if DEBUG
                print("Got bad size from read \(size)")
                #endif
                return
            }
            
            for index in 0...size {
                payload.append(payloadBuff.advanced(by: index).pointee)
            }
            
            payloadBuff.deallocate()
        }
        
        #if DEBUG
        print("--- BEGIN RECVD MESSAGE ---")
        print(payload.base64EncodedString())
        print("--- END MESSAGE ---")
        #endif
        let decoder = PropertyListDecoder.init()
        let encoder = PropertyListEncoder.init()
        
        var returnData: ModuleReturnData
        do {
            var format: PropertyListSerialization.PropertyListFormat = .binary
            let message = try decoder.decode(ModuleOptions.self, from: payload, format: &format)
            #if DEBUG
            print("Decoded: \(message)")
            #endif
            returnData = dispatch(message)
        } catch {
            #if DEBUG
            print("Decode error: \(error)")
            #endif
            returnData = .init(type: .comms, status: .BadFormat)
        }
        let returnBytes = try! encoder.encode(returnData)
        let returnBuff = UnsafeMutableRawBufferPointer.allocate(byteCount: returnBytes.count, alignment: 8)
        #if DEBUG
        print("--- BEGIN SEND MESSAGE ---")
        print(returnBytes.base64EncodedString())
        print("--- END SEND MESSAGE ---")
        #endif
        returnBytes.copyBytes(to: returnBuff)
        
        write(fd, returnBuff.baseAddress, returnBytes.count)
        returnBuff.deallocate()
    }
    
    public func run() -> Void {
        while true {
            var address = sockaddr()
            var addressSize: socklen_t = 0
            
            let fd = accept(sock_fd, &address, &addressSize)
            guard fd >= 0 else {
                #if DEBUG
                print("Failed to accept \(errno)")
                #endif
                continue
            }
            
            self.queue.async {
                #if DEBUG
                print("Handling client \(fd)")
                #endif
                self.handleClient(fd)
            }
            // zZz ...
            sleep(1)
        }
    }
}
