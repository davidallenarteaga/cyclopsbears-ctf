#import <stdlib.h>
#import <stdio.h>
#import <time.h>

#import "Player.h"

void get_line(char *buf, size_t bufsz, FILE *f)
{
    size_t bufTerm;

    fgets(buf, bufsz, f);
    for (bufTerm = 0; bufTerm < bufsz && buf[bufTerm] != '\0' && buf[bufTerm] != '\r' && buf[bufTerm] != '\n'; bufTerm++);
    buf[bufTerm] = '\0';
}

int main(int argc, const char *argv[])
{
    int result, running, consecutiveWins;
    enum Move playerMove, computerMove;
    FILE *fp;
    char buf[128];

    // Seed random
    srandom(time(NULL));

    // Create a new instance
    Player *p = [[Player alloc] init];
    printf("Please enter your name to begin: ");
    fflush(stdout);
    get_line(buf, sizeof(buf), stdin);
    [p setName: buf];

    running = 1;
    consecutiveWins = 0;
    while (running && !feof(stdin)) {
        printf("Please select a move (S/P/R): ");
        fflush(stdout);
        fgets(buf, sizeof(buf), stdin);

        switch(buf[0]) {
        case 'S':
        case 's':
            playerMove = SCISSORS;
            break;
        case 'P':
        case 'p':
            playerMove = PAPER;
            break;
        case 'R':
        case 'r':
            playerMove = ROCK;
            break;
        case 'q':
            [p printStats];
            running = 0;
            break;
        case '?':
            [p printStats];
            continue;
        default:
            printf("Unknown move, please select from: Scissors, Paper or Rock\n");
            fflush(stdout);
            continue;
        }

        if (!running) {
            break;
        }

        computerMove = [p getComputerMove];
        printf("The computer played: %s\n", computerMove == SCISSORS ? "Scissors" : computerMove == PAPER ? "Paper" : "Rock");
        fflush(stdout);

        result = [p playMove: playerMove computerMove: computerMove];
        if (result == 0) {
            printf("Tie!\n");
            fflush(stdout);
        } else if (result < 0) {
            printf("You lose!\n");
            fflush(stdout);
            consecutiveWins = 0;
        } else {
            printf("You win!\n");
            fflush(stdout);
            consecutiveWins++;
        }

        if (consecutiveWins > 32) {
            fp = fopen("flag.txt", "r");
            if (fp == NULL) {
                perror("Sorry, failed to open flag file");
                return 1;
            }
            get_line(buf, sizeof(buf), fp);
            fclose(fp);
            
            printf("You get a flag: %s\n", buf);
            fflush(stdout);
            running = 0;
            break;
        }
    }

    // Free memory
    [p release];

    return 0;
}
