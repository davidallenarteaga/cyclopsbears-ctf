cmake_minimum_required(VERSION 3.5)

project(Blinkenlights)

# Options
set(DEFAULT_FLAG "the_flag_is_cybears_curlybracealright_lets_get_this_out_onto_a_system_tray...nice!curlybrace")
set(FLAG "${DEFAULT_FLAG}" CACHE STRING "Flag to embed in binary")

find_package(PythonInterp)
message("Python: ${PYTHON_EXECUTABLE}")

# Generate the flag header
set(GENERATED_DIR ${CMAKE_CURRENT_BINARY_DIR})
add_custom_command(
    OUTPUT ${GENERATED_DIR}/flag.h
    COMMENT "Converting flag into morse code"
    COMMAND ${PYTHON_EXECUTABLE} "${CMAKE_CURRENT_SOURCE_DIR}/morse.py" "${GENERATED_DIR}/flag.h" "${FLAG}" "33"
)

# Compile the binary
add_executable(Blinkenlights
    WIN32
    "${GENERATED_DIR}/flag.h"
    Resource.rc
    resource.h
    main.cpp
)
target_include_directories(
    Blinkenlights
    PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${GENERATED_DIR}
)

if(MINGW)
    add_custom_command(
        TARGET Blinkenlights#
        POST_BUILD
        COMMAND strip --strip-all $<TARGET_FILE:Blinkenlights>
        COMMENT "Stripping debug symbols"
    )
endif()
