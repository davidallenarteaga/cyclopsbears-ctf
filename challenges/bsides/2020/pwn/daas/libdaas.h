unsigned int is_hex_string(unsigned char * s);

unsigned int is_odd_length(unsigned char *s);

unsigned int is_multiple_of_blocklen(unsigned char *s);

void convert_hex_to_bytes(unsigned char *s, unsigned char *output);

void handleErrors(void);

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext);

int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv, unsigned char *plaintext);

int decrypt_and_print_hex(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv);

int self_test();
