#!/usr/bin/env python3

'''
To run this you'll need the r2pipe library:
pip3 install r2pipe
'''
TARGET_FILE="neophyte"
MANIFEST_BASE="MANIFEST.yml.tpl"
MANIFEST="MANIFEST.yml"
SOLVE_BASE="solve.py.tpl"
SOLVE="solve.py"

import r2pipe
import json
import os
from string import Template
r = r2pipe.open(TARGET_FILE)
# analyse
r.cmd('aa')
# get the entry point
e_entry = json.loads(r.cmd('iej'))[0]['vaddr']
symbols = json.loads(r.cmd('isj')) # list the symbols
print_func = 0
main_func = 0
for s in symbols:
    if s['name'] == 'readFlag' and s['type'] == 'FUNC':
        print_func = s['vaddr']
        print("Found readFlag address at : 0x%x" % (print_func))
    if s['name'] == 'main' and s['type'] == 'FUNC':
        main_func = s['vaddr']
        print("Found main address at : 0x%x" % (main_func))
r.cmd('s main')
main_vars = json.loads(r.cmd('afvj'))

if len(main_vars['sp']):
    buffer = main_vars['sp'][0]
    return_address_offset = int(buffer["ref"]["offset"]) + 4
    print("Return address offset {}".format(hex(return_address_offset)))

# build our solver
with open(SOLVE_BASE, "r") as base:
    solver_base = Template(base.read())
    with open(SOLVE, "w") as solve:
        solve.write(solver_base.substitute(RETURN_ADDRESS_OFFSET=return_address_offset, PRINT_FLAG_ADDRESS=print_func))

    with open(os.path.join("handout",SOLVE), "w") as solve:
        solve.write(solver_base.substitute(RETURN_ADDRESS_OFFSET="# TODO: From previous answer", PRINT_FLAG_ADDRESS="# TODO: From previous answer"))

# populate the manifest with the correct answers
with open(MANIFEST_BASE, "r") as base:
    manifest_base = Template(base.read())
    with open(MANIFEST, "w") as manifest:
        #safe_substitute so $target_dns etc is left for later template operations.
        manifest.write(manifest_base.safe_substitute(e_entry=e_entry,
                                                     e_entry_hex=hex(e_entry), 
                                                     main_addr=main_func,
                                                     main_addr_hex=hex(main_func),
                                                     return_address_offset=return_address_offset,
                                                     return_address_offset_hex=hex(return_address_offset)))

r.quit()
