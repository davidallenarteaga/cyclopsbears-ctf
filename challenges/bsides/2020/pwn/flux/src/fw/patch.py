import sys

FILE_TO_PATCH = sys.argv[1]
FILE_WITH_FLAG = sys.argv[2]

# These must stay in sync with `app/main.c`
FLAG_START = b"cybears{"
FLAG_TERM = b"}"

with open(FILE_WITH_FLAG, "rb") as flag_file:
    new_flag = flag_file.read()

with open(FILE_TO_PATCH, "rb") as patch_file:
    data = bytearray(patch_file.read())

start = data.find(FLAG_START)
if start < 0:
    print(f"ERROR: No default flag!")
    sys.exit(1)

end = data.find(FLAG_TERM, start+len(FLAG_START))
if end < 0:
    print(f"ERROR: No flag terminator!")
    sys.exit(1)

end += 1 # Include the terminator in the length
old_len = end - start

if len(new_flag) > old_len:
    print(f"ERROR: New flag is longer than old flag!")
    sys.exit(1)

# Pad to length
new_flag += b'\0' * (old_len - len(new_flag))

data[start:end] = new_flag

open(FILE_TO_PATCH, "wb").write(data)
