#ifndef _CYBEARS_DRIVE_FLAG_H
#define _CYBEARS_DRIVE_FLAG_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "user.h"

#define FLAG_PATH "flag.txt"

char *flag = NULL;

void flag_get(void)
{
    if (NULL == current_user) {
        printf("Not logged in\n");
        return;
    }

    if ((strncmp(current_user->username, "admin", 5) != 0) || current_user->id != ADMIN_UID) {
        printf("Permission denied\n");
        return;
    }

    printf("%s\n", flag);
}

int flag_load(void)
{
    int flag_fd = -1;
    struct stat stat_buf = {0};
    ssize_t bytes_read = -1;
    ssize_t total_bytes_read = 0;

    if (0 != stat(FLAG_PATH, &stat_buf)) {
        printf("Error: Unable to open flag file. It should be placed at %s\n", FLAG_PATH);
        return 1;
    }

    /* Basic sanity checking. Effectively an arbitrary limit */
    if (stat_buf.st_size > 256) {
        printf("Error: Flag too large\n");
        return 1;
    }

    flag = calloc(1, stat_buf.st_size + 1); /* NULL Terminate */

    if (NULL == flag) {
        printf("Error: Unable to allocate space for flag\n");
        return 1;
    }

    if ((flag_fd = open(FLAG_PATH, O_RDONLY)) == -1) {
        printf("Error: Unable to open flag for reading\n");
        free(flag);
        return 1;
    }

    while (total_bytes_read < stat_buf.st_size) {
        bytes_read = read(flag_fd, &flag[total_bytes_read], stat_buf.st_size - total_bytes_read);

        if (-1 == bytes_read) {
            printf("Error: Flag read failure\n");
            free(flag);
            return 1;
        }

        total_bytes_read += bytes_read;
    }

    return 0;
}

void flag_free(void) {
    if (NULL != flag) {
        free(flag);
    }
}

#endif
