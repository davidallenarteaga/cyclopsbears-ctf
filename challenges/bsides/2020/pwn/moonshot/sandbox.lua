local charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
local function random_string(length)
    local str = ""
    for i=1,16 do
        local idx = math.random(1, #charset)
        str = str .. charset:sub(idx, idx)
    end
    return str
end

local DEBUG_PRINT = false
local LOG_FILE_NAME = "/logs/" .. random_string(16) .. ".log"
local LOG_FILE = io.open(LOG_FILE_NAME, "w+")

local function debug_log_print(data)
    if DEBUG_PRINT then
        print("Writing '" .. data .. "' to logfile '" .. LOG_FILE_NAME .. "'")
    end
end

function log(data)
    debug_log_print(data)
    LOG_FILE:write(data)
    LOG_FILE:flush()
end

local function clear_table(tbl)
    local keys = {}
    for k, _ in pairs(tbl) do
        table.insert(keys, k)
    end
    for _, v in ipairs(keys) do
        tbl[v] = nil
    end
end

debug.debug = nil
debug.getuservalue = nil
debug.setcstacklimit = nil
debug.setlocal = nil
debug.setupvalue = nil
debug.setuservalue = nil
debug.upvalueid = nil
debug.upvaluejoin = nil

collectgarbage = nil
dofile = nil
load = nil
loadfile = nil
require = nil

clear_table(io)
clear_table(os)
io = nil
os = nil
package.loaded.io = nil
package.loaded.os = nil
package.loadlib = nil
package.searchers[1] = function() print('This searcher has been disabled') return nil end
package.searchers[2] = function() print('This searcher has been disabled') return nil end
package.searchers[4] = function() print('This searcher has been disabled') return nil end
