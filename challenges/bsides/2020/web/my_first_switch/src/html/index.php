<?php

require_once("auth.php");

?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="layout.css" media="screen" />
<title> Cybear Networking Systems&copy;</title>
</head>

<body>

<div class="header">
	<h1>Network Status</h1>
</div>

<?php
	require("header.php");
?>

<div class="infobox">

	<h2>Status</h2>
	<p>
	<img src="images/green.svg" width="16px" height="16px"> Operational 
	<br />
	IP: 172.17.0.2
	<br />
	Netmask: 255.255.0.0
	<br />
	Gateway: 172.17.0.1
	<br />		
	</p>
	
</div>

<p></p>
<p></p>
<p></p>
<p></p>
<p></p>


<?php
	require("footer.php");
?>

</body>
</html>
