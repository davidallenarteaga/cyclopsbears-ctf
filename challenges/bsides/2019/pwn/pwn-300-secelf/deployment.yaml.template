apiVersion: apps/v1
kind: Deployment
metadata:
    name: $category-$name
    labels:
        challenge: $name
        category: $category
spec:
    replicas: $max_replicas # Scale this based on expected load/downtime if the challenge crashes
    selector:
        matchLabels:
            challenge: $name
    template:
        metadata:
            labels:
                challenge: $name
        spec:
            automountServiceAccountToken: false
            enableServiceLinks: false
            hostAliases:
                - ip: "127.0.0.1"
                  hostnames:
                    - $target_dns
            containers:
                # challenge container
                - name: $name
                  image: $server_container
                  imagePullPolicy: Always
                  ports:
                    $container_ports
                  volumeMounts:
                    # This is where the submissions are stored and run
                    - mountPath: /secelf-server/box/
                      name: submission-box
                  resources:
                      requests:
                        ephemeral-storage: "256M"
                # The second container in the pod runs a healthcheck, constantly solving the challenge
                - name: $name-test
                  image: $healthcheck_container
                  imagePullPolicy: Always
                  env:
                    - name: TERM
                      value: "xterm"
                    - name: TERMINFO
                      value: "/etc/terminfo"
                  # We need this because we don't quite fit their model
                  # We want a different container to do the test. They expect
                  # healthchecks from within the container. So we need this container
                  # to stay alive and pretend it's a service...
                  args:
                    - /bin/sh
                    - -c
                    - while true; do sleep 5; done
                  livenessProbe:
                      exec:
                          command:
                            - /bin/sh
                            - -c
                            - "$solve_script"
                      initialDelaySeconds: 5
                      periodSeconds: $healthcheck_interval
                      timeoutSeconds: 5
                  # Allow viewing errors in describe pods
                  terminationMessagePolicy: "FallbackToLogsOnError"
                  resources:
                    requests:
                        ephemeral-storage: "20M"
            # The secret here is a Gitlab deploy token with the read_registry permission
            imagePullSecrets:
                - name: regcred
            volumes:
                - name: submission-box
                  emptyDir: {}
                        
---
# This is the frontend for the challenge, the bit exposed to the internet
# It load balances between the instances of the challenge
apiVersion: v1
kind: Service
metadata:
    name: $name
    labels:
        challenge: $name
    annotations:
        cybears_dns: $target_dns
spec:
    # This creates a load balancer on AWS which costs extra money.
    # Use `kubectl get services -o wide` to get the domain name of the
    # loadbalancer, you can then hit it on port 2323.
    type: LoadBalancer
    ports:
        $service_ports
    selector:
        challenge: $name
