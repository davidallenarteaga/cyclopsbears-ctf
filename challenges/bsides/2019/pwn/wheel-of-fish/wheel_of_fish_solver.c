/**
 * @brief Wheel of Fish bruteforce solver
 **/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(int argc, char* argv[])
{
    uint32_t seed = 0;

    // Start with a known-printable seed
    uint32_t AAAA = 1094795585;
    for (seed = AAAA; seed < AAAA + 1000000; seed++)
    {
        srand(seed);

        // First spin:
        uint32_t spin = rand() % 12;
        if (spin != 1)
        {
            continue;
        }

        // What's in the box
        uint32_t box = rand() % 50;
        if (box != 3)
        {
            continue;
        }

        // This seed should get the red snapper and get the flag
        fprintf(stderr, "Found seed: %d\n", seed);

        // check if it is printable
        char seed_as_str[5] = {0};
        memcpy(seed_as_str, &seed, 4);

        bool printable = true;
        for (int i = 0; i < 4; i++)
        {
            printable = printable && isprint(seed_as_str[i]);
        }

        if (!printable)
        {
            fprintf(stderr, "-> non printable\n");
            continue;
        }

        // Build the input for the challenge
        // We need an extra character after the seed because otherwise the NULL byte
        // sets the number of spins to zero.
        char name[16] = {0};
        strcpy(name, "aaaabbbbccccd");
        memcpy(name + 8, seed_as_str, 4);
        printf("%s\nY\n", name);
        break;
    }

    return 0;
}
