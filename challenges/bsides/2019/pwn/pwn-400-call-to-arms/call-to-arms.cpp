using namespace std;
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// builder docker container needs "apt-get -y install g++-arm-linux-gnueabi"

class Soldier {
    public:
        Soldier(char *n, Soldier *p) : name(n), prev(p) {}
        Soldier *prev;
        Soldier *next = NULL;
        char *name;
        virtual void get_detail() = 0;
};

class Cybear : public Soldier
{
    public:
        Cybear(char *n, Soldier *p): Soldier(n,p) {}
        virtual void get_detail()
        {
            printf("Cybear %s\n", this->name);
        }
};

class DecepticomTSS : public Soldier
{
    public:
        DecepticomTSS(char *n, Soldier *p): Soldier(n,p) {}
        virtual void get_detail()
        {
            printf("DecepticomTSS %s\n", this->name);
        }
};

int menu(void)
{
    int c;
    printf("\nMenu\n\n"
            "1: Setup Name\n"
            "2: Create Soldier\n"
            "3: Remove Soldier\n"
            "4: Print Names\n"
            "5: Print Details\n"
            "6: Quit\n"
            "Press your selection:");
    do
    {
        c = getchar() - '0';
    }
#ifdef DEBUG
    while (c < 1 || c > 8);
#else
    while (c < 1 || c > 6);
#endif
    while(getchar() != '\n');

    return c;
}

int main(int argc, char *argv[], char* envp[])
{
    Soldier *first = NULL, *last = NULL;
    char *name = NULL;
    char temp_buffer[1024];

    int m;
    while ((m = menu()) != 6)
    {
        // These are just temp variables that is used in the switch cases
        register Soldier *u asm ("r9"); // This tells the compiler to keep this local variable in register r9
        int j;
        switch(m)
        {
            case 1:
                // Create buffer

                // free name if it has a buffer already
                if(name != NULL)
                    free(name);

                bzero(temp_buffer, sizeof(temp_buffer));

                printf("Enter name:");
                if(fgets(temp_buffer, sizeof(temp_buffer), stdin) == NULL)
                {
                    printf("Error reading name\n");
                    continue;
                }
                j = 0;
                while(j < sizeof(temp_buffer) && temp_buffer[j] != '\n') j++;
                if(j == sizeof(temp_buffer))
                {
                    printf("Name too long\n");
                    continue;
                }
                temp_buffer[j] = 0;
                name = (char *)malloc(j + 1);
                for(int i=0; i<=j; i++)
                    name[i] = temp_buffer[i];

                break;
            case 2:
                // Create entry
                if(name == NULL)
                {
                    printf("You must setup a name first\n");
                    continue;
                }

                j = 0;
                do
                {
                    printf("What type of soldier to create?\n1: Cybear\n2: DecepticomTSS\n");
                    if(scanf("%d", &j) == 0)
                    {
                        while(getchar() != '\n');
                    }
                }
                while(j != 1 && j != 2);

                printf("Creating entry for %s\n", name);
                if(j == 1)
                    u = new Cybear(name, last);
                else
                    u = new DecepticomTSS(name, last);

                name = NULL;

                if(first == NULL)
                    first = u;

                if(last != NULL)
                    last->next = u;

                last = u;
                break;
            case 3:
                // Remove entry

                do
                {
                    printf("What is the soldier number that you want to remove: ");
                    if(scanf("%d", &j) != 1 || j < 0)
                        continue;

                    u = first;
                    for(int i=0; i<j; i++)
                    {
                        if(u == last)
                        {
                            u = NULL;
                            break;
                        }

                        u = u->next;
                    }
                }
                while(u == NULL);

                printf("Removing %s\n", u->name);

                if(u == first && u == last)
                {
                    first = last = NULL;
                }
                else if (u == first)
                {
                    first = first->next;
                }
                else if (u == last)
                {
                    last = last->prev;
                }

                if(u->next && u->prev)
                {
                    u->prev->next = u->next;
                    u->next->prev = u->prev;
                }

                free(u->name);
                delete u;
                break;
            case 4:
                // Print Names
                if(first == NULL || last == NULL)
                    continue;

                u = first;
                j = 0;
                do
                {
                    printf("Soldier number %d: %s\n", j++, u->name);
                    u = u->next;
                } while (u != NULL);
                break;
            case 5:
                // Print Details
                if(first == NULL || last == NULL)
                    continue;

                u = first;
                j = 0;
                do
                {
                    printf("Soldier number %d: ", j++);
                    u->get_detail();
                    u = u->next;
                } while (u != NULL);
                break;
#ifdef DEBUG
            case 7:
                printf("Address of last - 0x%08X\n", last);
                break;
            case 8:
                printf("Address of name - 0x%08X\n", name);
                break;
#endif
        }
    }

    return 0;
}
