# negative_indexing
This is an implementation of a simple `signed char` misuse resulting in
negative indexing into an array.

The mechanics of the challenge are all here but it could stand to have a little
more though given to theming of why it's accepting data and why it needs to
deal with wide characters. It seems pretty reasonable to me to suggest that
this is a program written for the Decepticom or Cybeartronian mainframe which
needs to accept input in our native langauges or something.
