import requests
import json

#import logging - have INFO and DEBUG levels
#import argparse - have -h host -u user -p password -d debug level

import logging
import argparse
import time

import subprocess

#disable 'connecting to self-signed certificate' warning
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


#def login_user(user="test", password="test", host="http://127.0.0.1:5000", debug=False)
# return cookie json on success, None on failure
def login_user(user="alpha_trion", password="MyAltModeIsHovercraft", host="https://127.0.0.1:8443", debug=False):
	d = {"username":user, "password":password}
	r = requests.post(host + "/auth/login", data=d, verify=False)

	if r.status_code != 200:
		if debug:
			print("ERROR: status code: {}".format(r.status_code))
		return None
	
	return r.cookies.get_dict()	
	
#def show_users(cookie, host="http://127.0.0.1:5000", debug=False)
# return json users

def show_users(cookie, host="https://127.0.0.1:8443", debug=False):

	r = requests.get(host+"/api/show_users", cookies=cookie, verify=False)
	if r.status_code != 200:
		if debug:
			print("ERROR: status code: {}".format(r.status_code))
		return None

	return json.loads(r.content)

#def give_energon(cookie, recv_id, host="http://127.0.0.1:5000", debug=False)
# return success/failure code

def give_energon(cookie, recv_id, host="https://127.0.0.1:8443", debug=False):

    r = requests.get(host+"/give_energon?recv_id=" + str(recv_id), cookies=cookie, verify=False)
    if r.status_code != 200:
        if debug:
            print("ERROR: status code: {}".format(r.status_code))
        return None

    if r.content.find(b'ERROR') > -1: 
        print("ERROR: Error giving energon")
    else: 
        print("SUCCESS: successfully gave enrgon to {}".format(recv_id))

    return r.status_code

#def give_energon_early_data(cookie, recv_id, host="http://127.0.0.1:5000", cookie, debug=False)
# return success/failure code
# This will ONLY work on moonbase-1 docker image due to hardcoded paths for openssl 1.1.1a

def give_energon_early_data(cookie, recv_id, host="https://127.0.0.1:8443", debug=False):

    cookie_b64 = cookie['session']

	#GIVE FIRST ENERGON - create and save TLS session
    cmd = '''python -c "print('GET /give_energon?recv_id={} HTTP/1.0\\r\\nCookie: session={};\\r\\n\\r\\n')" | /usr/local/openssl-1.1.1a/bin/openssl s_client -tls1_3 --connect {}  -sess_out /tmp/sess --quiet'''.format(recv_id, cookie_b64, host.split("/")[2])
    ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
	
    if debug==True:
        print("DEBUG: " + output.decode("utf-8"))

	#create earlydata request
    f = open("/tmp/early.bin", "wb")
    req = "GET /give_energon?recv_id={} HTTP/1.0\r\nCookie: session={};\r\n\r\n".format(recv_id, cookie_b64)
    f.write(bytes(req, 'utf-8'))
    f.close()

	#GIVE SECOND ENERGON - EARLY DATA
    cmd2 = '''/usr/local/openssl-1.1.1a/bin/openssl s_client -tls1_3 --connect {}  -sess_in /tmp/sess -early_data /tmp/early.bin --quiet'''.format(host.split("/")[2])
    ps = subprocess.Popen(cmd2,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    if debug==True:
        print("DEBUG: " + output.decode("utf-8"))
    return 200
    
    
if  __name__ == "__main__":
    #logging.basicConfig(level=logging.DEBUG)
    #logging.debug("module loaded...")


    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--hostname', type=str,  default="https://teletraan1:443", help='server eg. https://teletraan1:443 or http://127.0.0.1:5000')
    parser.add_argument('-u', '--username', type=str,  default="alpha_trion", help='Username')
    parser.add_argument('-p', '--password', type=str,  default="MyAltModeIsHovercraft", help='Password')
    parser.add_argument('-l', '--loglevel', type=str, default="WARN", help='LogLevel=INFO|DEBUG|WARN|ERROR|CRITICAL')
    parser.add_argument('-e', '--earlydata', action='store_true', help='Use TLS1.3 early data to give energon')
    parser.add_argument('-t', '--sleeptime', type=int, default=60, help='sleep time (secs)')
    args = parser.parse_args()

    hostname = args.hostname
    username = args.username
    password = args.password
    loglevel = args.loglevel
    earlydata = args.earlydata
    sleeptime = args.sleeptime

    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    logging.basicConfig(level=numeric_level)
    
    
    while(True):
            
        #login
        cookie = login_user(user = username, password = password, host = hostname)
        
        if cookie == None: 
            logging.error("ERROR: Failed to login")
            time.sleep(sleeptime)
            break
        else: 
            logging.info("Successfully logged in")
        
        #get users
        j = show_users(cookie = cookie, host = hostname)
        if j == None: 
            logging.error("ERROR: Failed to fetch users")
            time.sleep(sleeptime)
            break
        else: 
            logging.info("Successfully polled users")
        
        if j['result'] != 'SUCCESS':
            logging.error("ERROR: json result not SUCCESS")
            time.sleep(60)
            break
        else: 
            logging.info("Successfully polled users with JSON")
            
        for user in j['users']:
        #if any users have <2 energon, give two energon 
            if user['energon'] < 2:       
                logging.info("USER [{}], id [{}] needs energon!".format(user['username'], user['id']))
                # early data
                if (earlydata == True):
                    r = give_energon_early_data(cookie=cookie, recv_id = user['id'], host = hostname, debug=True)
                    if r != 200: 
                        logging.error("ERROR in giving energon (early data)")
                    else:
                        #logging.info("Successfully gave energon to {} (early data)".format(user['id']))
                        logging.info("Successfully returned from give_energon (early data)")
                #not early data    
                else:
                    r = give_energon(cookie=cookie, recv_id = user['id'], host = hostname, debug=True)
                    if r != 200: 
                        logging.error("ERROR in giving energon")
                    else:
                        #logging.info("Successfully gave energon to {}".format(user['id']))
                        logging.info("Successfully returned from give_energo")



        
        #sleep for 60 secs
        time.sleep(sleeptime)
        



            



