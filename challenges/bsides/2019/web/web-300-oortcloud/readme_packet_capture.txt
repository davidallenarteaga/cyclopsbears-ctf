I've placed a few captures here while I'm debugging my new TLS1.3 server. 

The following tcpdump capture line will capture 5 minutes (-G 300) and write it out to a file (-w). The %M will add a minute string to the filename, which will end up providing a cyclic pcap log that should have a maximum of 12 files. The only problem is that the files won't be in capture-time order, you'll just need to look at the timestamps to grab the most recent capture. 

tcpdump -G 300 -w '/usr/share/nginx/html/pcap/teletraan-1-%M.pcap' -s0 'port 443'

 - Cyberbook admin-bot
