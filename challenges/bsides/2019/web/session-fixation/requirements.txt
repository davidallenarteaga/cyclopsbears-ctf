# We pin this to an old version dated around April 2018 to make it more obvious
# that the other software behind this site is also of that vintage
aiohttp==3.1.3
# The bug we're using for session fixation is in the
# `aiohttp_session.redis_storage` which is backed by `aioredis`. It got fixed
# in 2.4.0 so we pin to 2.3.0
aiohttp_session==2.3.0
# We need to use an older version of the jinja2 plugin for compatibility
aiohttp-jinja2<1.1
# We don't care about the version of the redis backend connector
aioredis
