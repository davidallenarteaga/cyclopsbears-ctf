VERSION 5.00
Begin VB.Form frmAccessDenied
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "ACCESS DENIED"
   ClientHeight    =   1695
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6795
   Icon            =   "frmAccessDenied.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   1695
   ScaleWidth      =   6795
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1
      Interval        =   500
      Left            =   6240
      Top             =   1200
   End
   Begin VB.Shape Shape1
      BorderColor     =   &H000000FF&
      BorderWidth     =   4
      Height          =   1215
      Left            =   390
      Top             =   240
      Width           =   6015
   End
   Begin VB.Label Label1
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ACCESS DENIED"
      BeginProperty Font
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   555
      Left            =   1342
      TabIndex        =   0
      Top             =   570
      Width           =   4110
   End
End
Attribute VB_Name = "frmAccessDenied"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_KeyPress(KeyAscii As Integer)
Unload Me
End Sub

Private Sub Form_Load()
Me.Left = Screen.Width / 2 - Me.Width / 2
Me.Top = Screen.Height / 2 - Me.Height / 2
End Sub

Private Sub Timer1_Timer()
Shape1.Visible = Not Shape1.Visible
End Sub

