/**
 * @brief Works On My Machine PE section deobfuscation
 **/

#include "worksonmymachine.h"
#include <windows.h>
#include <stdint.h>
#include <stdio.h>

// Deobfuscate section with XOR
bool DoDeobfuscation(const char * section_name, const char * key, const size_t key_size)
{
    bool success = false;
    HMODULE hModule = GetModuleHandle(NULL);
    PIMAGE_DOS_HEADER dos_header = (PIMAGE_DOS_HEADER)hModule;
    PIMAGE_NT_HEADERS nt_header = (PIMAGE_NT_HEADERS)((uint8_t*) hModule + dos_header->e_lfanew);
    PIMAGE_SECTION_HEADER section_header = IMAGE_FIRST_SECTION(nt_header);
    DWORD old_protect = 0;

    for (uint32_t section_id = 0; section_id < nt_header->FileHeader.NumberOfSections; section_id++)
    {
        if (_stricmp(section_name, (const char*)(section_header->Name)) == 0)
        {
            // Deobfuscate this section
            uint8_t * section_start = (uint8_t*)hModule + section_header->VirtualAddress;
            size_t section_size = section_header->SizeOfRawData;

            if (VirtualProtect(section_start, section_size, PAGE_READWRITE, &old_protect))
            {

                for (size_t i = 0; i < section_size; i++)
                {
                    if (section_start[i] != 0)
                    {
                        section_start[i] = section_start[i] ^ key[i % key_size];
                    }
                }
                VirtualProtect(section_start, section_size, old_protect, &old_protect);
            }
            success = true;
            break;
        }

        section_header++;
    }

    // HACK: undo this later
    success = (nt_header->FileHeader.NumberOfSections > 0);
    return success;
}