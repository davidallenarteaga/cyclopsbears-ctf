; The MBR will load the main code from disk and jump into it

BITS 16
ORG 0x7C3E

CODE_LOCATION_SECTOR        equ     3

start:
    cli
    xor ax, ax              ; Set data segment to 0
    mov ds, ax
    mov es, ax
    
load_main:
    mov bx, 0x7E00
    mov ah, 0x02                    ; function: read sectors
    mov al, 3                       ; number of sectors
    xor ch, ch                      ; tracks
    mov cl, CODE_LOCATION_SECTOR    ; sector number
    xor dh, dh                      ; heads
    int 13h
    
jump_to_main:
    jmp 0x7E00