# Bear Metal

## Build

Dependencies include nasm, mtools, python and pip. QEMU and The Sleuth Kit are required for the automated tests. The included Dockerfile will build a container with everything you need to build and test Bear Metal.

Use `build.sh` to build the disk image. This script will format an empty floppy disk image, place the flag file in the image, assemble the shellcode, then patch it over the top of the image's existing MBR.

Use `test.sh` to test the image. This script will use QEMU to boot up the image, send keystrokes to simulate entering the correct key, then extract the flag file from the filesystem using The Sleuth Kit.

## Solution

### File system

```
user@ctf:/mnt/c/ctf/cybears/testing$ file bearmetl.img
bearmetl.img: DOS/MBR boot sector, code offset 0x3c+2, OEM-ID "mkfs.fat", root entries 224, sectors 2880 (volumes <=32 MB) , sectors/FAT 9, sectors/track 18, serial number 0xaabbccdd, label: "CYBEARMETAL", FAT (12 bit), followed by FAT
user@ctf:/mnt/c/ctf/cybears/testing$ fls bearmetl.img
r/r 3:  CYBEARMETAL (Volume Label Entry)
r/r 4:  GARBAGE.TXT
v/v 45779:      $MBR
v/v 45780:      $FAT1
v/v 45781:      $FAT2
```

The disk image contains a single file, "garbage.txt", which has the message "This is not the flag you are looking for" followed by garbage.

### MBR

The disk has a custom MBR that loads the main bootloader into memory and executes it.

### Bootloader

The bootloader prompts for an eight character key. It then does a series of checks on the key. If the checks pass, the key is XORed against the contents of the flag file on the filesystem.

There are 16 checks. Each check takes some combination of characters from the key and adds them together to get a constant value. You could manually solve these, or use an SMT solver. A sample solution using Z3 is included in `solve.py`.

The password is "ZvbXrp1-".

### File recovery

Once you have booted the disk with the correct key, you can recover the flag file from the disk. The original message at the start of the file is lost, however the rest of the file is now a valid ZIP file. The ZIP file contains a single file that holds the flag.

```
user@ctf:/mnt/c/ctf/cybears/testing$ fls bearmetl.img
r/r 3:  CYBEARMETAL (Volume Label Entry)
r/r 4:  GARBAGE.TXT
v/v 45779:      $MBR
v/v 45780:      $FAT1
v/v 45781:      $FAT2
user@ctf:/mnt/c/ctf/cybears/testing$ icat bearmetl.img 4 > GARBAGE.TXT
user@ctf:/mnt/c/ctf/cybears/testing$ file GARBAGE.TXT
GARBAGE.TXT: data
user@ctf:/mnt/c/ctf/cybears/testing$ binwalk GARBAGE.TXT

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
40            0x28            Zip archive data, at least v2.0 to extract, compressed size: 105, uncompressed size: 106, name: f
223           0xDF            End of Zip archive, footer length: 22
user@ctf:/mnt/c/ctf/cybears/testing$ unzip GARBAGE.TXT
Archive:  GARBAGE.TXT
warning [GARBAGE.TXT]:  40 extra bytes at beginning or within zipfile
  (attempting to process anyway)
  inflating: f
user@ctf:/mnt/c/ctf/cybears/testing$ cat f

The flag is: cybears{s0meone_3D_pr1nted_t3h_s@ve_ic0n}
```
