#!/usr/bin/env python3
import sys

def main():
    if len(sys.argv) < 4:
        print("usage: %s dest_file src_file offset_hex [max_size_hex]" % sys.argv[0])
        return
        
    dest_file = sys.argv[1]
    src_file = sys.argv[2]
    offset = int(sys.argv[3], 16)
    max_size = int(sys.argv[4], 16) if len(sys.argv) > 4 else None
    
    print(" - Patching %s into %s at offset 0x%x" % (src_file, dest_file, offset))
    
    src_data = bytearray(open(src_file, "rb").read())
    dest_data = bytearray(open(dest_file, "rb").read())
    
    src_data_len = len(src_data)
    print(" - Source data is 0x%x bytes" % src_data_len)
    
    if max_size:
        if src_data_len > max_size:
            raise Exception("patch too big!")
            
    dest_data[offset:offset+len(src_data)] = src_data
    with open(dest_file, "wb") as dest_file_out:
        dest_file_out.write(dest_data)
    
if __name__ == "__main__":
    main()