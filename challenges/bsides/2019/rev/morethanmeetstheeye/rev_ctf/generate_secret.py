import struct
import Crypto.Cipher.ARC4
import Crypto.Hash.SHA256

def encrypt(key, plaintext):
    sha = Crypto.Hash.SHA256.new()
    sha.update(key)

    rc4 = Crypto.Cipher.ARC4.new(sha.digest())
    return rc4.encrypt(plaintext)

flag = b"cybears{Linux_in_disgu1s3}\0"


# Max length of 8 characters
key = b"2.6.35.1"

ciphertext = encrypt(key, flag)

encrypted_flag = '{' + ', '.join(hex(char) for char in ciphertext) + '}'

header_string = """
#ifndef __SECRET_H__
#define __SECRET_H__

CONST BYTE SECRET[] = {};

#endif
"""

header_string = header_string.format(encrypted_flag)

with open("secret.h", "w") as f:
    f.write(header_string)


print(key)
print(encrypt(key, ciphertext))
