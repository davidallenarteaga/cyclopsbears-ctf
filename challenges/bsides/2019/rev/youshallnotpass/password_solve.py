
#python2
#pip install z3

from z3 import *

flag_start = "cybears{"
flag_end = "}"
len_flag = 23

s = Solver()
#initiate instances
x=[]
for i in range(0,len_flag):
    x.append(Int('x'+str(i)))

#add printable ASCII contraints to solver
for i in range(0,len_flag):
    s.add( x[i] >= 32 )
    s.add( x[i] < 127 )

#add known components of flag
def addKnown(s, x, flag_start, flag_end):
    for i in range(0, len(flag_start)):
        s.add( x[i] == ord(flag_start[i]))

    for i in range(0, len(flag_end)):
        s.add( x[len(x)-len(flag_end) + i] == ord(flag_end[i]))

addKnown(s,x,flag_start, flag_end)
s.add(x[11]*x[3]-x[4]*x[17]==-1511)
s.add(x[9]-x[5]*x[14]+x[16]==-4992)
s.add(x[21]-x[22]*x[6]==-14312)
s.add(x[16]*x[21]*x[4]-x[5]==323769)
s.add(x[11]-x[15]*x[4]*x[6]==-546512)
s.add(x[13]-x[16]-x[1]-x[13]==-174)
s.add(x[0]-x[19]*x[17]*x[6]==-598131)
s.add(x[15]+x[11]*x[14]+x[8]==3866)
s.add(x[19]*x[21]*x[5]==366282)
s.add(x[1]+x[14]-x[4]-x[14]==24)
s.add(x[10]-x[8]-x[22]==-112)
s.add(x[1]*x[1]-x[18]==14592)
s.add(x[4]-x[15]*x[17]-x[19]==-4952)
s.add(x[14]*x[12]*x[21]==184275)
s.add(x[19]*x[22]-x[9]==6290)
s.add(x[7]*x[16]+x[8]==6601)
s.add(x[3]*x[1]+x[13]*x[10]==20201)
s.add(x[6]+x[11]+x[21]==261)
s.add(x[20]+x[12]-x[16]==112)
s.add(x[14]*x[21]-x[10]*x[10]==-6190)
s.add(x[0]*x[17]+x[6]==10213)
s.add(x[9]-x[16]-x[14]-x[3]==-114)
s.add(x[18]*x[11]+x[14]+x[6]==4227)
s.add(x[6]*x[15]-x[21]==5572)
s.add(x[14]+x[1]-x[2]+x[14]==113)
s.add(x[9]*x[16]-x[15]+x[11]==4539)
s.add(x[1]*x[10]*x[16]==609235)
s.add(x[6]-x[6]-x[5]+x[12]==-49)
s.add(x[13]-x[19]*x[13]==-4200)
s.add(x[14]+x[19]-x[9]-x[19]==-40)
s.add(x[21]*x[11]*x[1]-x[20]==632609)
s.add(x[22]+x[5]+x[18]==288)
s.add(x[20]-x[4]+x[17]+x[22]==230)
s.add(x[7]+x[11]-x[11]+x[11]==206)
s.add(x[19]*x[8]+x[14]+x[11]==4310)
s.add(x[4]*x[21]*x[1]+x[10]==739526)
s.add(x[5]+x[3]-x[18]-x[3]==65)
s.add(x[6]*x[4]+x[5]==11269)
s.add(x[14]*x[14]*x[17]-x[16]==206497)
s.add(x[4]-x[3]+x[22]+x[1]==242)
s.add(x[18]*x[21]+x[5]-x[13]==3117)
s.add(x[11]+x[8]*x[11]==6889)
s.add(x[8]-x[16]-x[11]+x[8]==28)
s.add(x[8]-x[12]*x[11]-x[12]==-5378)
s.add(x[18]+x[10]*x[11]*x[11]==654504)
s.add(x[16]-x[16]*x[12]*x[3]==-347892)

print(s.check())
mod = s.model()

output = ""

for i in range(0,len_flag):
    output += (chr(int(str(mod[x[i]]))))

print output

e=''
for i in range(0, len(output)):
        e += "x["+str(i) + "]==" + str(ord(output[i])) + ","

s.add(Not(And(eval(e[:-1]))))

if str(s.check()) == 'unsat':
    print("Unique Solution!")
else:
    print("Non-unique solutions exist... more work needed")

