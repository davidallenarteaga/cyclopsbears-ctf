
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

int main(int argc, char *argv[])
{

	char * x = NULL;
	int result = TRUE; 
	int password_length = 0;

	if(argc != 2)
	{
		printf("Usage: a.out <password>\n");
		return -1;
	}

	printf("argv[1]: %s, strlen(password) = %d\n", argv[1], strlen(argv[1]));

	x = argv[1];

	password_length = strlen(x);
	if (password_length != 23)
	{
		printf("Incorrect password length\n");
		return -1;
	}

	if (!(x[11]*x[3]-x[4]*x[17]==-1511))
	{
		result = FALSE;
	}
	if (!(x[9]-x[5]*x[14]+x[16]==-4992))
	{
		result = FALSE;
	}
	if (!(x[21]-x[22]*x[6]==-14312))
	{
		result = FALSE;
	}
	if (!(x[16]*x[21]*x[4]-x[5]==323769))
	{
		result = FALSE;
	}
	if (!(x[11]-x[15]*x[4]*x[6]==-546512))
	{
		result = FALSE;
	}
	if (!(x[13]-x[16]-x[1]-x[13]==-174))
	{
		result = FALSE;
	}
	if (!(x[0]-x[19]*x[17]*x[6]==-598131))
	{
		result = FALSE;
	}
	if (!(x[15]+x[11]*x[14]+x[8]==3866))
	{
		result = FALSE;
	}
	if (!(x[19]*x[21]*x[5]==366282))
	{
		result = FALSE;
	}
	if (!(x[1]+x[14]-x[4]-x[14]==24))
	{
		result = FALSE;
	}
	if (!(x[10]-x[8]-x[22]==-112))
	{
		result = FALSE;
	}
	if (!(x[1]*x[1]-x[18]==14592))
	{
		result = FALSE;
	}
	if (!(x[4]-x[15]*x[17]-x[19]==-4952))
	{
		result = FALSE;
	}
	if (!(x[14]*x[12]*x[21]==184275))
	{
		result = FALSE;
	}
	if (!(x[19]*x[22]-x[9]==6290))
	{
		result = FALSE;
	}
	if (!(x[7]*x[16]+x[8]==6601))
	{
		result = FALSE;
	}
	if (!(x[3]*x[1]+x[13]*x[10]==20201))
	{
		result = FALSE;
	}
	if (!(x[6]+x[11]+x[21]==261))
	{
		result = FALSE;
	}
	if (!(x[20]+x[12]-x[16]==112))
	{
		result = FALSE;
	}
	if (!(x[14]*x[21]-x[10]*x[10]==-6190))
	{
		result = FALSE;
	}
	if (!(x[0]*x[17]+x[6]==10213))
	{
		result = FALSE;
	}
	if (!(x[9]-x[16]-x[14]-x[3]==-114))
	{
		result = FALSE;
	}
	if (!(x[18]*x[11]+x[14]+x[6]==4227))
	{
		result = FALSE;
	}
	if (!(x[6]*x[15]-x[21]==5572))
	{
		result = FALSE;
	}
	if (!(x[14]+x[1]-x[2]+x[14]==113))
	{
		result = FALSE;
	}
	if (!(x[9]*x[16]-x[15]+x[11]==4539))
	{
		result = FALSE;
	}
	if (!(x[1]*x[10]*x[16]==609235))
	{
		result = FALSE;
	}
	if (!(x[6]-x[6]-x[5]+x[12]==-49))
	{
		result = FALSE;
	}
	if (!(x[13]-x[19]*x[13]==-4200))
	{
		result = FALSE;
	}
	if (!(x[14]+x[19]-x[9]-x[19]==-40))
	{
		result = FALSE;
	}
	if (!(x[21]*x[11]*x[1]-x[20]==632609))
	{
		result = FALSE;
	}
	if (!(x[22]+x[5]+x[18]==288))
	{
		result = FALSE;
	}
	if (!(x[20]-x[4]+x[17]+x[22]==230))
	{
		result = FALSE;
	}
	if (!(x[7]+x[11]-x[11]+x[11]==206))
	{
		result = FALSE;
	}
	if (!(x[19]*x[8]+x[14]+x[11]==4310))
	{
		result = FALSE;
	}
	if (!(x[4]*x[21]*x[1]+x[10]==739526))
	{
		result = FALSE;
	}
	if (!(x[5]+x[3]-x[18]-x[3]==65))
	{
		result = FALSE;
	}
	if (!(x[6]*x[4]+x[5]==11269))
	{
		result = FALSE;
	}
	if (!(x[14]*x[14]*x[17]-x[16]==206497))
	{
		result = FALSE;
	}
	if (!(x[4]-x[3]+x[22]+x[1]==242))
	{
		result = FALSE;
	}
	if (!(x[18]*x[21]+x[5]-x[13]==3117))
	{
		result = FALSE;
	}
	if (!(x[11]+x[8]*x[11]==6889))
	{
		result = FALSE;
	}
	if (!(x[8]-x[16]-x[11]+x[8]==28))
	{
		result = FALSE;
	}
	if (!(x[8]-x[12]*x[11]-x[12]==-5378))
	{
		result = FALSE;
	}
	if (!(x[18]+x[10]*x[11]*x[11]==654504))
	{
		result = FALSE;
	}
	if (!(x[16]-x[16]*x[12]*x[3]==-347892))
	{
		result = FALSE;
	}

	if (result == TRUE)
	{
		printf("CONGRATULATIONS!\n");
		return 0;
	}

	if (result == FALSE)
	{
		printf("Incorrect password\n");
		return 0;
	}

	return 0;
}
