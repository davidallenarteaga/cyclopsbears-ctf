misc-200-Anagram
=================

* _title_: Fear Mr Snort
* _points_: 200
* _tags_: misc, crypto

## Flags
* `cybears{N0@m_3lk135_4nn0y1ng_4d@g3s}`

## Challenge Text
```markdown
It may not seem obvious, but in the list of Pokemon (TM) toys listed below, the following subsets of names anagram to each other
`Spearow + Dugtrio + Abra + Rapidash + Slowpoke + Hitmonlee + Seadra + Kabuto + Moltres = Arbok + Poliwrath + Doduo + Seel + Horsea + Starmie + Lapras + Kabutops + Dragonite + Mew`

The above anagram has a subset of 9 names, anagramming to another subset of 10. In the attached set of 50 Transformers (TM) names, find an anagram with at least 12 names on one side.

Connect via `nc misc.cybears.io 1729` to submit. Submissions should be in a single line, the two halves should be delimited with a `=` and each name should be delimited with a `+`

```

## Attachments
* `Transformers.txt` : List of top Transformers names in order of number of toys produced

## Hints
*

## References
* http://www2.unicron.com/characterlist.html?sort=toys
* `cat TransformersNames.txt | cut -d "." -f 2 | cut  -f 2`
* https://www.ams.org/meetings/in-cooperation-with/elkies
* http://www.alexhealy.net/anagram.html
* http://www.alexhealy.net/papers/anagram.pdf

## Humourous anagrams of Transformers
* https://new.wordsmith.org/anagram/anagram.cgi?anagram=transformer&t=500&a=n
* Errant Forms
* Rant Reforms
* Fear Mr Snort
* Rent Or Farms
* Mr Fart Snore
* Mr Fat Snorer
* For Mars Rent

<details>
<summary>SPOILERS - Walkthrough</summary>
<p>

## Walkthrough
* Googling "subset anagram" -> second page links to this academic paper:
* http://www.alexhealy.net/papers/anagram.pdf
* describes Noam Elkies lattice basis reduction to find subset anagrams
* Can either implement algorithm in paper, or go to the website listed in paper:
* http://www.alexhealy.net/anagram.html
* Note that if you enter the 50 names into the webiste directly, it will only give you an anagram subset of size 11 - not 12
* If you enter random subsets of size about 40, sometimes there is an anagram of size >= 12 that you can use

## Solver
* You can also run solve script in sage
* sage available on Docker registry as
* https://hub.docker.com/r/sagemath/sagemath/
* docker run -it sagemath/sagemath:latest
* Or I think you can run sage online
* `load("misc_200_FearMrSnort_solution.sage")` should give you the following two solutions
* `OPTIMUSPRIME+RATCHET+BARRICADE+ARCEE+HOUND+LOCKDOWN+STRAFE+SMOKESCREEN+RAVAGE+BLADES+THRUST+RAMJET+JOLT = STARSCREAM+DEVASTATOR+JETFIRE+BLACKOUT+STRONGARM+CHEETOR+BONECRUSHER+DEADEND+WHEELJACK+OPTIMUSPRIMAL`
* `GRIMLOCK+PROWL+BARRICADE+ULTRAMAGNUS+JETFIRE+SHOCKWAVE+SMOKESCREEN+MUDFLAP+HEATWAVE+BREAKDOWN+CYCLONUS+JOLT = MEGATRON+STARSCREAM+SOUNDWAVE+CLIFFJUMPER+ARCEE+MIRAGE+HOUND+LOCKDOWN+BLACKOUT+SKYWARP+WHEELJACK+SILVERBOLT`

</p>
</details>
