"""
This module contains a base class that defines some kind of problem that
should be worked on.

The actual implementation of the problem should extend this class and implement
the function definitions.
"""


class BaseProblem:
    """
    This is a base class for a CTF challenge problem.
    The server calls the methods here when running through a problem.
    """
    # pylint: disable=too-many-instance-attributes
    def __init__(self):
        # How long to wait after the text is shown
        self.splash_screen_wait = 2
        self.problem_statement_wait = 0
        self.problem_setup_wait = 0
        self.loading_message = "Loading"
        self.problem_timeout = 10
        self.connection_timeout_message = '\nConnection closed!'
        self.buffer_size = 4096
        self.problem_setup_exception_message = '\nThere was an error.'

    def get_splash_screen_text(self):
        """
        This should return a dictionary that looks like this:

            {
                'message': <your formatted string REQUIRED>,
                'split_message': <if you want the message to be split up on
                                  transmission into substrings>,
                'delimiter': <what to split the string on>,
                'row_delay': <the delay in seconds (float) between sending each
                              split string bit>
            }

        The split_message will tell the send function in the server module
        that you want to send chunks of your message split by the delimiter
        every row_delay period. It's so you can make it a little cooler.
        """
        raise NotImplementedError

    def get_problem_statement_text(self):
        """
        This is where you define any extra problem statement information that
        doesn't fit in the splash screen. If there is nothing here it won't
        print.

        Same return dictionary as get_splash_screen_text
        :return:
        """
        raise NotImplementedError

    #pylint: disable=no-self-use
    def setup_problem(self, attempts=0):
        """
        This is where the problem is created; and any extra information is
        sent as a result of creating the problem as a msg dictionary like
        get_splash_screen_text
        :return:
        """
        del attempts
        return False, {}

    #pylint: disable=unused-argument
    def check_client_solution(self, client_solution):
        """
        Check the solution provided (do validation etc) and I've they've got
        the answer; give them a cookie.

        Response is in the standard format above:

            {
                'message': <your formatted string (if they're good put a flag>,
                'split_message': <if you want the message to be split up on
                                  transmission into substrings>,
                'delimiter': <what to split the string on>,
                'row_delay': <the delay in seconds (float) between sending each
                              split string bit>
            }

        :param response:
        :return:
        """
        return {
            'message': 'IM NOT DONE YET!',
        }


# Import the created challenge
# pylint: disable=wrong-import-position, cyclic-import
from maximum_extract_challenge.challenge import MaximumExtractProblem

# Set this attribute to the concrete implementation of the problem
ChallengeClass = MaximumExtractProblem
