"""
This module is a separate hived off test because it's not super reliable
and totally dependent on the machine it's run on. As such, I don't include
it in the pytest step.

But feel free to pytest it and watch things break.
"""
import time
import logging
import pytest
import math
import socket
import errno
from multiprocessing import Process
from multiprocessing import Queue
from solution.solution import solve_problem
from server import main as run_server
from server import CHALLENGE_FLAG
from server_settings import HOST_SOCKET
from maximum_extract_challenge.challenge import MaximumExtractProblem, \
    INVALID_INPUT_ERROR, GREEDY_SOLUTION_DETECTED_ERROR, \
    FILE_VALUE_NOT_MAXIMISED_ERROR, DUPLICATE_SELECTION_ERROR


logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(f'TESTER')


def solution_worker(result_queue, worker_number=None,
                    alternate_message=None, delay=None):
    """
    Wrap the solution function as a worker so we can turn
    it into a separate process that returns a result via
    a multiprocessing Queue.
    :param result_queue:
    :param worker_number:
    :param alternate_message:
    :param delay:
    :return:
    """
    result = solve_problem(alternate_message, delay)
    result_queue.put((worker_number, result))


class TestServerDenialOfService(object):
    """
    Test that making a huge bunch of connections over a period of time
    doesn't crash the server and that you can still solve the problem at
    the end of the day.
    """

    def setup_class(self):
        MaximumExtractProblem.DIFFICULTY_SCALE = 25

        # Test if the socket is available, and retry if it isn't
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(("127.0.0.1", HOST_SOCKET))
            sock.close()
            self.server_process = Process(target=run_server)
            self.server_process.start()
            time.sleep(1)  # Give the server a sec to start before continuing
        except socket.error as ex:
            if ex.errno == errno.EADDRINUSE:
                LOGGER.error("Port is already in use... waiting 5 seconds and"
                             " retrying...")
                time.sleep(5)
                self.setup_class(self)
            else:
                # something else raised the socket.error exception
                LOGGER.error(f'{ex}')
                raise

    def teardown_class(self):
        self.server_process.terminate()
        self.server_process.join()

    def test_many_connections(self, caplog):
        """
        Just make a large number of connections and time out; then
        give a crack at a single solve to see if the server is still works
        after all that.
        :return:
        """
        caplog.set_level(logging.INFO)
        total_clients = 1000
        connection_batches = 20
        clients_per_batch = math.ceil(total_clients / connection_batches)
        batch_wait_time = 4
        result_queue = Queue()
        client_number = 0
        clients = []

        # Make a bunch of connections in batches with small waits between
        for _ in range(connection_batches):
            for _ in range(clients_per_batch):
                client = Process(target=solution_worker,
                                 args=(result_queue,
                                       client_number,
                                       f'DENIAL_MESSAGE_{client_number}',
                                       None,))

                LOGGER.info(f'Starting client {client_number}')
                client_number += 1
                clients.append(client)
                client.start()

            time.sleep(batch_wait_time)

        # Return the processes
        for client in clients:
            client.join()

        # We don't care about the DOS results
        del result_queue
        time.sleep(1)  # Give old mate a moment

        # Finally, test that the server can still respond
        server_feedback = solve_problem()
        assert (CHALLENGE_FLAG == server_feedback)
