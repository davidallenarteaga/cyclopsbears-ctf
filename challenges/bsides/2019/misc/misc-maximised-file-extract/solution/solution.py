"""
This is a solution for the puzzle.

NOTE: I've structure this in a way that I can also use it for my integration
testing, so there is a whole bunch of extra bells and whistles with timeouts
and putting in random messages / delays that you don't really need.

It connects to the server, reads out the important information (the JSON doc)
and solves using the appropriate algorithm then sends a comma separated
variable back to the server.
"""
import argparse
import json
import time
import logging
import os
import socket

HOST_SOCKET = int(os.environ.get("HOST_SOCKET", "2323"))
DOWNLOAD_LIMIT = 1440000


def greedy_solver(file_list, download_limit):
    """
    Simple check to see if the greedy solution is the best.

    It's a bit of a dick move, but I'll validate the DP solution above
    against this and disconnect if the solution can be done via a greedy
    method (it'd because I'm a prick).

    It shouldn't happen often if ever, but on the off chance it does it'll
    make life a little shittier for the person trying to solve this.

    :param file_list:
    :param download_limit:
    :return:
    """
    # Sort the list from most valuable to least valuable
    file_list = sorted(file_list, key=lambda x: x[-1], reverse=True)
    file_size = 0
    selection = []

    # Get the files from most valuable to least valuable
    for file in file_list:

        if (file_size + file[1]) > download_limit:
            pass
        else:
            selection.append(file)
            file_size += file[1]

    return None, selection


def recursive_solution(download_limit, selected, files, i):
    """
    My recursive solution for the problem. This is the same algorithm that
    the server is using. Because the server is doing a little more work
    (it solves this + greedy + has a slight 10% penalty added) this will beat
    it before it closes the connection.

    To read about the problem:
    http://www.es.ele.tue.nl/education/5MC10/Solutions/knapsack.pdf

    :param download_limit:
    :param selected:
    :param files:
    :param n:
    :return:
    """
    # Get the nth file
    file = files[i - 1]
    file_size = file[1]
    file_value = file[-1]

    # If we have no more items or hit the download limit; return
    if i == 0 or download_limit == 0:
        return 0, selected

    # If that item will make it over capacity, move onto the next item
    if file_size > download_limit:
        return recursive_solution(download_limit,
                                  selected,
                                  files,
                                  (i - 1))

    # Otherwise do the following:
    #  1. get the value and selection including the nth item
    #  2. get the value and selection excluding the nth item
    # Return the situation of maximum value.
    new_download_limit = download_limit - file_size
    value_a, selected_a = recursive_solution(new_download_limit,
                                             list(),
                                             files,
                                             (i - 1))
    value_a += file_value  # The new value including the file

    # Option 2: exclude the nth item.
    value_b, selected_b = recursive_solution(download_limit,
                                             list(),
                                             files,
                                             (i - 1))

    # Return the added item if the value is more
    if value_a > value_b:
        selected_a.append(file)
        return value_a, selected_a

    return value_b, selected_b


# This is a straight script to solve the problem; I'm not worried about
# the code quality for these disabled items here.
# pylint: disable=too-many-locals, too-many-branches, too-many-statements
def solve_problem(alternate_message=None, delay=None, greedy=False):
    """
    This method connects to and solves the problem. It can be used to test
    different ways of trying to solve the problem.

    - if alternate_message is not None then send it.
    - if delay is not none, wait before sending.
    - if greedy is True, try a greedy algorithm to solve it.

    :param alternate_message:
    :param delay:
    :param greedy:
    :return:
    """

    # Connect to the server
    sock = socket.socket()
    sock.settimeout(30)
    try:
        sock.connect(('127.0.0.1', HOST_SOCKET))
    except socket.timeout:
        logger = logging.getLogger(f'{os.getpid()}:CLIENT')
        logger.error('FAILED_TO_CONNECT')
        return 'FAILED_TO_CONNECT'

    client_address = sock.getsockname()

    logger = logging.getLogger(f'{os.getpid()}:'
                               f'CLIENT_{client_address[0]}'
                               f':{client_address[1]}')
    logger.info('Connecting to server')

    # Get ready to strip out the json_data
    json_data = ""
    collect = False
    cursor_count = 0

    # Listen to the splash screen and data coming back and quit when we hit
    # the second >>
    long_timeout = 60  # Time out for the total process
    long_start_time = time.time()  # Get the start time for a timeout
    sock.settimeout(10)
    input_timeout = 20  # Timeout for when we receive the CONNECTED banner
    check_timeout = False

    logger.info(f'Receiving Data...')
    continue_problem = False
    while True:

        batch = ''
        try:
            batch = sock.recv(4096).decode('utf-8')
            logger.debug(f'{batch[:-1]}')
        except socket.timeout:
            logger.debug(f'socket recv timed out')

        if '::CONNECTED::' in batch:
            start_time = time.time()  # Get the start time for a timeout
            check_timeout = True

        # Collect the info between the tags
        if 'END_FILE_LIST' in batch:
            collect = False
            continue_problem = True
        if collect:
            json_data += batch
        if 'START_FILE_LIST' in batch:
            collect = True

        # Exit collection
        if '>>' in batch:
            if cursor_count >= 1:
                break

            cursor_count += 1

        # Break if the input recv timeout was hit
        if check_timeout and (time.time() - start_time) > input_timeout:
            logger.info(f'Data capture timed out.')
            break

        # Break if the process has stalled
        if (time.time() - long_start_time) > long_timeout:
            logger.info(f'Process timed out.')
            break

    if continue_problem:
        # Convert the data into an array for our algorithm
        data = json.loads(json_data)
        file_list = []
        for key, value in data.items():
            file_list.append((key,
                              value.get('file_size'),
                              value.get('file_value')))

        if delay:
            logger.debug(f'Inserting test delay {delay}')
            time.sleep(delay)

        # Used for testing alternative stuff to send
        if alternate_message:
            # Send our comma separated string to the server!
            logger.debug(f'Sending alternate message: {alternate_message}')
            try:
                sock.sendall(alternate_message.encode('utf-8'))
                server_feedback = sock.recv(4096).decode('utf-8')
                logger.info(f'Received from server: {server_feedback}')
            except BrokenPipeError:
                return 'BROKEN_PIPE'
        else:
            # Solve the problem (quickly)
            if greedy:
                logger.info(f'Solving with greedy.')
                solution = greedy_solver(file_list,
                                         DOWNLOAD_LIMIT)
            else:
                logger.info(f'Solving with dynamic.')
                solution = recursive_solution(DOWNLOAD_LIMIT,
                                              list(),
                                              file_list,
                                              len(file_list))
            solution_string = ', '.join([x[0] for x in solution[-1]])
            logger.debug(f'Calculated solution: {solution_string}')

            # Send our comma separated string to the server!
            sock.sendall(solution_string.encode('utf-8'))
            server_feedback = sock.recv(4096).decode('utf-8')
            logger.debug(f'Received from server: {server_feedback}')

            if 'flag{' in server_feedback:
                logger.info(f'FLAG CAPTURED! {server_feedback}')
            else:
                logger.error(f'No flag found :(')
                logger.info(f'Server replied: {server_feedback}')

        sock.close()
        return server_feedback

    sock.close()
    return 'NO_RESULT'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", type=int, help="server port")
    parser.add_argument("-d", "--delay", type=int, help="delay before responding")
    parser.add_argument("-m", "--message", help="custom message to send - won't solve")
    parser.add_argument("-g", "--greedy", action="store_true", help="greedy solve")
    args = parser.parse_args()
    print(args)
    if args.port:
        HOST_SOCKET = args.port
    # Solve the problem
    solve_problem(
        greedy=args.greedy, alternate_message=args.message, delay=args.delay
    )
