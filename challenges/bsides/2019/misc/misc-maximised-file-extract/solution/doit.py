#!/usr/bin/env python3
"""
This is a solution for the puzzle.

NOTE: This is the simplified one that isn't used with testing
"""
# pylint: disable=invalid-name
import argparse
import json
import time
import logging
import os
import socket


parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', required=True, help='The host:port of the remote server to run against')
args = parser.parse_args()

DOWNLOAD_LIMIT = 1440000

logger = logging.getLogger(f'SOLVER')

def recursive_solution(download_limit, selected, files, i, sol_depth):
    """
    To read about the problem:
    http://www.es.ele.tue.nl/education/5MC10/Solutions/knapsack.pdf

    :param download_limit:
    :param selected:
    :param files:
    :param n:
    :return:
    """
    # Get the nth file
    file = files[i - 1]
    file_size = file[1]
    file_value = file[-1]

    # If we have no more items or hit the download limit; return
    if i == 0 or download_limit == 0:
        return 0, selected

    # If that item will make it over capacity, move onto the next item
    if file_size > download_limit:
        return recursive_solution(download_limit,
                                  selected,
                                  files,
                                  (i - 1),
                                  sol_depth + 1)

    # Otherwise do the following:
    #  1. get the value and selection including the nth item
    #  2. get the value and selection excluding the nth item
    # Return the situation of maximum value.
    new_download_limit = download_limit - file_size
    value_a, selected_a = recursive_solution(new_download_limit,
                                             list(),
                                             files,
                                             (i - 1),
                                             sol_depth + 1)
    value_a += file_value  # The new value including the file

    # Option 2: exclude the nth item.
    value_b, selected_b = recursive_solution(download_limit,
                                             list(),
                                             files,
                                             (i - 1),
                                             sol_depth + 1)

    # Return the added item if the value is more
    if value_a > value_b:
        selected_a.append(file)
        # Uncomment to diagnose the algo, but likely miss the cutoff
        # logger.debug(f'DEPTH_{sol_depth} SEL_A: A_{value_a}, B_{value_b},'
        #              f' CHOICES: {selected_a}')
        return value_a, selected_a

    # Uncomment to diagnose the algo, but likely miss the cutoff
    # logger.debug(f'DEPTH_{sol_depth} SEL_B: A_{value_a}, B_{value_b},'
    #              f' CHOICES: {selected_b}')
    return value_b, selected_b


"""
DO IT!
"""
# Setting to debug will show the algo logic, but miss the flag (printing is
# slow)
logging.basicConfig(level=logging.INFO)

# Connect to the server
sock = socket.socket()
hostname, port = args.remote.split(':')
logger.info("Connecting to {} {}".format(hostname, int(port)))
sock.connect((hostname, int(port)))
#client_address = sock.getsockname()
logger.info('Connecting to server')

# Get ready to strip out the json_data
json_data = ""
collect = False
cursor_count = 0

# Listen to the splash screen and data coming back and quit when we hit
# the second >>
long_timeout = 60  # Time out for the total process
long_start_time = time.time()  # Get the start time for a timeout
sock.settimeout(10)
input_timeout = 20  # Timeout for when we receive the CONNECTED banner
check_timeout = False

logger.info(f'Receiving Data...')
continue_problem = False
while True:

    batch = ''
    try:
        batch = sock.recv(4096).decode('utf-8')
        logger.info(f'{batch[:-1]}')
    except socket.timeout:
        logger.debug(f'socket recv timed out')

    if '::CONNECTED::' in batch:
        start_time = time.time()  # Get the start time for a timeout
        check_timeout = True

    # Collect the info between the tags
    if 'END_FILE_LIST' in batch:
        collect = False
        continue_problem = True
    if collect:
        json_data += batch
    if 'START_FILE_LIST' in batch:
        collect = True

    # Exit collection
    if '>>' in batch:
        if cursor_count >= 1:
            break

        cursor_count += 1

    # Break if the input recv timeout was hit
    if check_timeout and (time.time() - start_time) > input_timeout:
        logger.info(f'Data capture timed out.')
        break

    # Break if the process has stalled
    if (time.time() - long_start_time) > long_timeout:
        logger.info(f'Process timed out.')
        break

if continue_problem:
    # Convert the data into an array for our algorithm
    data = json.loads(json_data)
    file_list = []
    for key, value in data.items():
        file_list.append((key,
                          value.get('file_size'),
                          value.get('file_value')))

    logger.info(f'Solving with dynamic.')
    solution = recursive_solution(DOWNLOAD_LIMIT,
                                  list(),
                                  file_list,
                                  len(file_list),
                                  0)
    solution_string = ', '.join([x[0] for x in solution[-1]])
    logger.debug(f'Calculated solution: {solution_string}')

    # Send our comma separated string to the server!
    sock.sendall(solution_string.encode('utf-8'))
    server_feedback = sock.recv(4096).decode('utf-8')
    logger.debug(f'Received from server: {server_feedback}')

    if 'cybears{' in server_feedback:
        logger.info(f'FLAG CAPTURED! {server_feedback}')
    else:
        logger.info(f'No flag found :(')

sock.close()
