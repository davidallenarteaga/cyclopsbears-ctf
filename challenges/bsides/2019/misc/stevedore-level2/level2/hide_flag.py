#!/usr/local/bin/python3

import os, random
import string, shutil

# Our script
verify_script = os.path.join(os.path.dirname(os.path.realpath(__file__)),'verify_flag.py')

# Our flag from cybears{xxxxxxxxxxxxxxxxx}
with open('flag.txt', 'r') as f:
    flag = f.read().split('{')[1].split('}')[0]

# Hide the real flag dir (70001 folders - a prime number as a tiny hint)
# Create one additional just like in create_flag_maze.py
randpath = os.sep.join(x for x in flag)

while os.path.isdir(randpath):
    randpath = os.sep.join(random.sample(string.digits, 10))
os.makedirs(randpath)
shutil.copy2(verify_script, os.path.join(randpath, 'verify_flag.py'))

'''
Rules for hiding the flag:
    directory owner OR group permissions will allow user to:
        read (list files); and
        execute (enter directory)
Root user will always be able to see everything anyway, but will 
really only have permissions for (randomly) half. CTF user will have
permission for the other half.

Other user permissions (not owner/group) will only be both read and 
execute (ie. 5 or 7) for the final flag directory parts. This won't be 
visually obvious, and since other users don't really exist in docker land 
it should be hard to test.

Since permissions aren't set in the script left in the docker container, it 
should be apparent that the hide_flag.py alters directory permissions in 
some meaningful way.

Browse the directory structure as some 'other' user and solve:
# find . -type f 2>&1 | grep verify
'''

user  = '01234567'
group = ['57','57','57','57','57','01234567','57','01234567']
other = '01234567'

# u = user, g = group, o = other
for root, dirs, files in os.walk('.', topdown=False):
    for d in dirs:

        u = random.choice(user)
        g = random.choice(group[int(u)])
        if root.count(os.sep) > 4: 
            o = random.choice('12346')
        else:
            o = random.choice(other)
        
        if random.randint(0,1) == 0:
            os.chmod(os.path.join(root, d), int(u + g + o, 8))
        else:
            # Reverse user/group half the time
            os.chmod(os.path.join(root, d), int(g + u + o, 8))

# Fix the specific flag dir so 'other' has perms to see/run
for i in range(len(flag)):
    u = random.choice(user)
    g = random.choice(group[7])
    o = random.choice('57')
    os.chmod(os.sep.join(x for x in flag[:i+1]), int(u + g + o, 8))
    
print("Flag hidden")
