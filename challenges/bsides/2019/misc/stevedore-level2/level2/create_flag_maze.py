#!/usr/local/bin/python3

import os, random
import string, shutil

# Our script
verify_script = os.path.join(os.path.dirname(os.path.realpath(__file__)),'verify_flag.py')

# Make the maze (not too large - otherwise dockerhub fails to build)
for i in range(59998):
    randpath = '.'
    while os.path.isdir(randpath):
        randpath = os.sep.join(random.sample(string.digits, 10))
    os.makedirs(randpath)
    shutil.copy2(verify_script, os.path.join(randpath, 'verify_flag.py'))
