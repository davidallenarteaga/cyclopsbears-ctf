"""
This is a programattic work through of the challenge which will check that
the outputs are what I am kinda expecting to see.
"""
import os
import subprocess
import zipfile
import re
from builder import build_challenge
from multiprocessing import Process
from src.tools import text_to_chicken, chicken_interpreter, brainfuck
from src.tools.pathlang import path

python3_exec = 'python3'
python2_exec = 'python2'

# Build the challenge
try:
    build_challenge()
except FileNotFoundError:
    pass  # Try solving the challenge anyway

current_folder = os.path.dirname(os.path.realpath(__file__))
handout_folder = os.path.join(current_folder, r'handout')
solution_dir = os.path.join(current_folder, r'solution')
source_dir = os.path.join(current_folder, r'src')

print('STARTING SOLUTION:\n')

"""
Flag 1:

    The note given gives a huge red flag as to what the password could be
    and states:

     "the techs said they found a post-it note with "chicken" written on it
     under the keyboard"

     Note: this password is also in the seclists 500-worst-passwords.txt so
     getting it should be easy.
"""
handout_archive = os.path.join(handout_folder, 'chicken.zip')
password_guess = 'chicken'

# Extract the archive with our guess
zipfile.ZipFile(handout_archive).extractall(path=solution_dir,
                                            pwd=password_guess.encode('utf-8'))

# List the new contents
extracted_files = []
print('File list:')
for (dirpath, dirnames, filenames) in os.walk(solution_dir):
    for filename in filenames:
        file_info = os.stat(os.path.join(dirpath, filename))
        print(f'File: {filename}, Size (bytes): {file_info.st_size}')
    break

# The resulting files should be:
#     File: chickn.py, Size (bytes): 15338
#     File: flag.chn, Size (bytes): 18634
#     File: r_and_d_notes.chn, Size (bytes): 30714561
# The file flag.chn can be decoded easily with the online chicken interpreter
# but we'll use our pre-made interpreter to get it out...
flag_chn_file = os.path.join(solution_dir, 'flag.chn')
print(f'Converting flag.chn...')
flag_1 = chicken_interpreter.do_chicken(flag_chn_file)
print(f'\nFound flag 1: {flag_1}\n')

"""
Flag 2:

    The bigger chicken file doesn't work on the online interpreters that I've
    tried, and I couldn't find one that did work... so this bit requires us
    to fix the broken interpreter.

    Lucky for me I know whats wrong...
"""
path_chn_file = os.path.join(solution_dir, 'r_and_d_notes.chn')
interpreter_file = os.path.join(solution_dir, 'chickn.py')

# Lets load the python file as a string
with open(interpreter_file, 'r') as fp:
    interpreter_lines = fp.readlines()

# The problem with the source code is in the op codes for multiply and add
# this is called by the lambdas on lines 41 and 42
print(f'Line 41: {interpreter_lines[41][:-1]}')
print(f'Line 42: {interpreter_lines[42][:-1]}')

# The easiest way to fix this is to replace the function calls in the lambdas
# with the operations... Line 41 is supposed to be multiply, and line 42 is
# supposed to be add
interpreter_lines[41] = interpreter_lines[41].replace(
    'chiCKEN(Chicken, chicken)',
    'Chicken * chicken'
)
interpreter_lines[42] = interpreter_lines[42].replace(
    'ChiCKEN(Chicken, chicken)',
    'Chicken + chicken'
)
print(f'Fixed Line 41: {interpreter_lines[41][:-1]}')
print(f'Fixed Line 42: {interpreter_lines[42][:-1]}')

# Save the fixed file and then import and call it
with open(interpreter_file, 'w') as fp:
    fp.write(''.join(interpreter_lines))

# Interpret the big chicken file
rc = subprocess.call([python3_exec,
                      interpreter_file,
                      path_chn_file])

# There should be a new file in our list
print('\nFile list:')
for (dirpath, dirnames, filenames) in os.walk(solution_dir):
    for filename in filenames:
        file_info = os.stat(os.path.join(dirpath, filename))
        print(f'File: {filename}, Size (bytes): {file_info.st_size}')
    break

# Yep; there is.... r_and_d_notes.chn.out, Size (bytes): 58924
path_chn_out_file = os.path.join(solution_dir, 'r_and_d_notes.chn.out')

# Lets have a look at the top of the file:
print('\nOutput of the new file:\n')
with open(path_chn_out_file, 'r') as fp:
    path_chn_out_lines = fp.readlines()
print(''.join(path_chn_out_lines[:18]))  # Oh look! Another flag!
flag_2 = re.sub('(\+| )', '', path_chn_out_lines[1])
print(f'\nFound flag 2: {flag_2}\n')

"""
Flag 3:

    The new file has a number of hints in it as to what it is. The most
    obvious hint being that the FILENAME at the top of the file in its text
    is listed as "r_and_d_notes.path" and it contains information about a
    PATH programming language.

    Fortunately we have a path interpreter. Although the people doing this
    will have to figure out where to get one (there are a few out there)
"""

# If you try to execute this file it'll get stuck with the output:
# BRAINFUCK:
# The problem is the program gets stuck in an infinite loop in the document
# on line 399-400
with open(path_chn_out_file, 'r') as fp:
    path_chn_out_lines = fp.readlines()

# We can fix the loop by simply making it jump past the loop (1 char fix)
print('Broken part of the PATH doc:\n')
print(''.join(path_chn_out_lines[396:401]))
path_chn_out_lines[398] = path_chn_out_lines[398].replace('>', '!')
print('Fixed part of the PATH doc:\n')
print(''.join(path_chn_out_lines[396:401]))

# Save the fixed version and run it; find what the output is
with open(path_chn_out_file, 'w') as fp:
    fp.write(''.join(path_chn_out_lines))

path_interpreter = os.path.join(source_dir, 'tools/path.py')

# Try and execute the fixed program
out = subprocess.check_output([python2_exec,
                               path_interpreter,
                               path_chn_out_file]).decode('utf-8')

# It should print a bunch of brainfuck prefixed with "BRAINFUCK:" now...
print('Fixed PATH file output:\n')
print(out)

# Lets extract the brainfuck code. If you run it, it'll get stuck printing "cybears{"
bf_src = out.replace('BRAINFUCK: ', '')

# The place it gets stuck is in an infinite input loop at [,] ... so we can
# simply replace this with nothing and run it!
bf_src = bf_src.replace('[,]', '')

# Save the fixed version and run it; find what the output is
brainfuck_out_file = os.path.join(solution_dir, 'flag.bf')
with open(brainfuck_out_file, 'w') as fp:
    fp.write(bf_src)

bf_interpreter = os.path.join(source_dir, 'tools/brainfuck.py')
flag_3 = subprocess.check_output([python3_exec,
                                  bf_interpreter,
                                  brainfuck_out_file]).decode('utf-8')


print(f'\nFound flag 3: {flag_3}\n')

# Assert that the flags are all in and correct
with open('flag.txt', 'r') as fp:
    flags = fp.readlines()

if flag_1 not in flags[0]:
    raise Exception('Flag 1 is incorrect.')

if flag_2 not in flags[1]:
    raise Exception('Flag 2 is incorrect.')

if flag_3 not in flags[2]:
    raise Exception('Flag 3 is incorrect.')
