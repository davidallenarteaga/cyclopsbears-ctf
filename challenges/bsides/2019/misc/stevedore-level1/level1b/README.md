Level 1b
---

Greetings again friend,

There is still no way I can get data to you outside of these special containers.
Please do your best to discover the secret in each - our safety depends on it.

The export security restrictions mean only verified builds can be shipped out -
so the way we communicated last time won't work. You'll have all the layers that
make up the image, as well as a copy of the file used to create it. 

You'll need to combine your results for the message to become clear. Good luck,

/ Bumblebear

p.s. This is part b of level1
