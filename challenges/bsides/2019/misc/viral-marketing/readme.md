# Viral Marketing


## Handout

handout/ViralMarketing.zip

## Running

None


<details>
<summary>SPOILERS</summary>
<p>


Challenge around NTFS filsystem things, mainly alternative data streams

## Setup

!!!WARNING!!!

Done in a Windows VM with a small vmdk connected and set to D:\
This will format your D:\ if you don't want to do that don't run this script

    cmake -G "Visual Studio 15 2017 Win64" .
    cmake --build . --config Release
    cd setup
    setup.bat

    1. Shut down the VM
    2. zip the vmdk to compress.
    3. check-in the new zip.

## Solver
    Attach the vmdk to a Windows VM. Tested with Windows 10 VM in Virtualbox. Make sure it's mounted as D: Or modify the solve.bat script to the correct drive.

    solve.bat


## Steps / Hints

   1. Find executable (easy)
   1. RE exe find call to first stream on "*" which is '.' directory.
   1. ADS on directory for password. Password should be easy to get.
   1. 'Hidden' ADS when ends in '. ' - does not show up with streams.exe or dir /r
   1. You can move/delete/open files ending with ' ' by nt path '\\\\?\C:\path\to\file '