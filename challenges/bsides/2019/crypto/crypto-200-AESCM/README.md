crypto-200-DCM
=================

* _title_: DCM
* _points_: 200
* _tags_: crypto

## Flags
* `cybears{|3 /-\ |) ( 0 |_| |\| + 3 R}`

## Challenge Text
```markdown
We've managed to recover a DecepticomTSS secure comms program and an encrypted transmission... but, we don't have the passphrase used to encrypt it. Can you help?

```

## Attachments
* `dcm.py` : program to encrypt/decrypt
* `DecepticomTSS_Message.txt.enc` : encrypted data

## Hints
*

## References

## Notes
* To generate plain (this will embed the flag into random plaintext. Ensure that you keep the plaintext roughly the same length, as players need a particular length of padding to solve. Also, don't change the offset of the flag into the plaintext. Note that players won't recover the FULL plaintext, only partial - keep this in mind when modifying plaintext)
`python3 generate_plain.py > plain.txt`
* To build cipher:
`python3 dcm.py -i plain.txt -m encrypt -p "passphrase" > plain.txt.enc`
* To test decrypt
`python3 dcm.py -i plain.txt.enc -m decrypt -p "passphrase" `
* To run solve script:
`python3 dcm_solve.py -i plain.txt.enc`


## Walkthrough
* Guess that there's some padding in the plain text
* Use that to get some chunk of possible key
* Notice that the implementation of the counter mode is bad,
* and ends up reusing keys, creating reuse between different sessions
* Try the key from above in all the sessions, profit


