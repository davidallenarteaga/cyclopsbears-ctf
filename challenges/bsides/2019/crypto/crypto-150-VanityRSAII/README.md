crypto-150-VanityRSAII
=================

* _title_: Vanity Keys - Part II
* _points_: 150 (maybe this should be 200?)
* _tags_: crypto

## NOTE
**This challenge should be time released for the start of day 2**

## Flags
* `cybears{M4yb3_w3_5h0uld_u53_3CC?}`

## Challenge Text
```markdown
Cybears comms officer cipher has realised that the last RSA private key he told you to create is too easy to forge. 

You will need to regenerate your private key again. This time you will need to generate a new 1024-bit RSA public/private key, ensuring that the string "cybearsctf" is embedded in the LOWER half of the RSA public modulus.  

You will need to submit your answer in X.509 format.  

Submit your key here: `nc crypto.cybears.io 1414`
```

## Attachments
*

## Hints
* 

## References
* https://arxiv.org/pdf/0709.2704.pdf (On RSA Moduli with Almost Half of the Bits Prescribed)

