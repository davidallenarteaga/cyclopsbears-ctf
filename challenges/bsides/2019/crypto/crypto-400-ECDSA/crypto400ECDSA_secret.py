import ecdsa
from random import SystemRandom

randrange = SystemRandom().randrange

#flag
flag = "cybears{T4k3_th4t_pl4yst4t10n}"

#Setup elliptic curve
g = ecdsa.ecdsa.generator_192
n=g.order()

#generate secret key
#secret = randrange(2,n-1)
secret = 1994348211405174788490195270566451173565455158685587023532

pubkey = ecdsa.ecdsa.Public_key(g, g*secret)
privkey = ecdsa.ecdsa.Private_key(pubkey, secret)
