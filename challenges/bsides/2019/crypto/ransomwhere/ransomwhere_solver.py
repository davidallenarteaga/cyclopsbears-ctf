import io
import os
import subprocess
import sys
import tempfile

# When python added 3.7.10 this broke our CI for ransomwhere
# uncompyle relies on a package called xdis which does the bytecode disassembly
# it needs specific version numbers from python to do the disassembly correctly
# but it hasn't been updated enough. 
# the following lines add python 3.7.10 support for xdis, then importing uncompyle
# see https://www.python.org/downloads/release/python-3710/
# https://github.com/rocky/python-xdis/blob/master/xdis/magics.py#L345
from xdis.magics import add_canonic_versions
add_canonic_versions("3.7.10", "3.7.0")

import uncompyle6

EXPECTED_FLAG = b"cybears{l3t5_c@u53_s0m3_m@yh3m!}"

source = io.StringIO()
uncompyle6.decompile_file("./handout/ransom.pyc", outstream=source)
source.seek(0)
(pubkey_line, ) = (l for l in source.readlines() if "BEGIN PUBLIC" in l)
# Hope it's not evil ;)
eval_dict = dict()
exec(pubkey_line.strip(), eval_dict, eval_dict)
pubkey = eval_dict["public_key_str"]
print("[-] Extracted public key\n%s" % pubkey)

RSACTFTOOL_PATH = os.environ["RSACTFTOOL_PATH"]
privkey_fname = tempfile.mktemp()
with tempfile.NamedTemporaryFile(mode="w") as f:
    f.write(pubkey)
    f.flush()
    subprocess.check_call((
        sys.executable, os.path.join(RSACTFTOOL_PATH, "RsaCtfTool.py"),
        "--publickey", f.name, "--attack", "fermat", "--private",
        "--output", privkey_fname,
    ))
with open(privkey_fname, "r") as privkey_file:
    print("[-] Recovered private key\n%s" % privkey_file.read())

# Creates ThreatIntellReport.pdf.enc.dec
subprocess.check_call((
    sys.executable, "./handout/ransom.pyc",
    "-i", "./handout/ThreatIntellReport.pdf.enc",
    "-m", "decrypt", "-v", privkey_fname,
))
print("[-] Decrypted PDF document")

# Annoyingly, pdf2txt doesn't include a shebang so we can't just run it
pdf2txt_script = subprocess.check_output(("which", "pdf2txt.py")).strip()
output = subprocess.check_output((
    sys.executable, pdf2txt_script, "./handout/ThreatIntellReport.pdf.enc.dec",
))
print("[-] Extracted PDF text lines\n%s" % output.decode())
assert EXPECTED_FLAG in output
print("SUCCESS! Test passed")
