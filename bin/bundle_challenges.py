#!/usr/bin/env python3
import argparse
import os
import os.path
import pathlib
import re
import string
import unicodedata
import yaml
import zipfile

import sys
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
LIB_DIR=os.path.join(BASE_DIR,'lib')
sys.path.append(LIB_DIR)
from challenge import Challenge
from utils import discover_manifests

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BUNDLE_DIR = os.path.normpath(os.path.join(SCRIPT_DIR, "..", ".bundle"))

def load_manifest(manifest_path):
    print("Loading challenges from {}".format(manifest_path))
    yield from Challenge.from_manifest(manifest_path)

def slugify(value):
    value = unicodedata.normalize('NFKD', value)
    value = value.encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)

# https://stackoverflow.com/a/33300001
# It seems like the pipe string style doesn't always take in the dumped
# manifest but that's okay, it's just a little jank ;)
def str_presenter(dumper, data):
    if len(data.splitlines()) > 1:  # check for multiline string
        return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
    return dumper.represent_scalar('tag:yaml.org,2002:str', data)
yaml.add_representer(str, str_presenter)

def build_bundle(challenge, manifest_path, releasable=False):
    print("Bundling challenge release for %r" % challenge.name)
    manifest = b''
    if releasable:
        # Rebuild a releasable MANIFEST from the `Challenge` object
        manifest = yaml.dump({
            "challenge": {
                "name": challenge.name,
                "category": challenge.category,
                "tags": challenge.tags,
                "description": challenge.description,
                "handouts": list(h.path for h in challenge.handouts),
            }
        })
    else:
        # Rebuild a full MANIFEST from the `Challenge` object
        # we do this rather than dumping the original manifest
        # as its possible to have multiple challenges in one MANIFEST
        chal_dict = challenge.dump()
        chal_dict["handouts"] = list(h.path for h in challenge.handouts)
        if challenge.deployment:
            chal_dict["deployment"] = challenge.deployment.dump()
        manifest = yaml.safe_dump({"challenge": chal_dict })

    # Now create the bundle!
    challenge_bundle_fname = os.extsep.join((slugify(challenge.name), "zip"))
    challenge_bundle_path = os.path.join(BUNDLE_DIR, challenge_bundle_fname)
    try:
        os.makedirs(BUNDLE_DIR)
    except FileExistsError:
        pass
    with zipfile.ZipFile(challenge_bundle_path, mode="w") as bundle:
        bundle.writestr(
            os.path.join(slugify(challenge.name), "MANIFEST.yml"), manifest
        )
        for handout in challenge.handouts:
            bundle.writestr(
                os.path.join(slugify(challenge.name), handout.path),
                handout.file.read()
            )

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--releasable", '-r', action='store_true', help="Bundle a releasable (sans flag etc.) version")
    parser.add_argument(
        "search_path", nargs="*", type=pathlib.Path,
        default=(pathlib.Path(__file__).parent.parent / "challenges", ),
    )
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    for manifest_path in discover_manifests(args.search_path):
        for challenge_obj in load_manifest(manifest_path):
            build_bundle(challenge_obj, manifest_path, args.releasable)
