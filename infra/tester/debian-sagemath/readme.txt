A number of crypt challenges require sage for testing/solving. I was originally having issues with getting the official sagemath/sagemath docker image to work in gitlab CI so built a debian image with "apt get sagemath". This official docker is about 750Mb compressed, while the debian one is 1.5Gb compressed. 

This set of dockerfiles builds both and pushes to cybears gitlab registry for use by other projects. This should also speed up some CI tests. 
 - Dockerfile.debian_sagemath: Base debian image with sagemath package installed 
   - tag "debian-sagemath"
 - Dockerfile.sagemath_sh: Official sagemath/sagemath image, changes default shell back to /bin/sh
   - tag "sagemath-sh"

-cybears.cipher
