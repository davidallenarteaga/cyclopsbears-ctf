#!/usr/bin/env bash

set -o errexit

echo "What is the deploy token username?"
read TOKEN_NAME
echo "What is the deploy token password?"
read -s PASSWORD

echo "Creating!"
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=${TOKEN_NAME} --docker-password=${PASSWORD} --docker-email=deploy@cybears.io
echo "The new secret is called \"regcred\""
