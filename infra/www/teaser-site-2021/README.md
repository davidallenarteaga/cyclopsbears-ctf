# Cybears 2021 landing site

Written using gatsby for no real reason other than I wanted to learn it. It also had a [simple deploy to S3](https://www.gatsbyjs.com/docs/how-to/previews-deploys-hosting/deploying-to-s3-cloudfront/) which we wanted to use for hosting this year.

1. `git clone <this repo>`
1. `npm install`
1. `pip install aws`
1. `aws configure` - this will require having an IAM token setup for your S3 bucket
1. Ensure the gatsby-config.js contains your bucket name and domain name suitable for your own deployment.
1. `npm run build && npm run deploy`
