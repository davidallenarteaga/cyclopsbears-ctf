import os
import CTFd.config

class Config(CTFd.config.Config):
    """
    We provide this override of the CTFd config class as a drop-in.
    """
    # XXX: What blows up if we generate this every time we roll the app?
    SECRET_KEY = os.urandom(64)

    # We accept backend info from the environment and explode if its absent
    SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URL"]
    CACHE_TYPE = "redis"
    CACHE_REDIS_URL = os.environ["REDIS_URL"]

    # Cookie config - these are Flask things
    SESSION_COOKIE_HTTPONLY = True
    SESSION_COOKIE_SAMESITE = "strict"

    # Outbound mail settings - we don't actually expect to use this...
    MAILFROM_ADDR = "donotreply@cybears.io"

    # Data storage locations
    UPLOAD_PROVIDER = "filesystem"
    UPLOAD_FOLDER = os.environ["UPLOAD_FOLDER"]
    LOG_FOLDER = os.environ["LOG_FOLDER"]

    # Other junk
    UPDATE_CHECK = False
    APPLICATION_ROOT = "/"
    # Just turn off Major League Cyber integration
    OAUTH_CLIENT_ID = None
    OAUTH_CLIENT_SECRET = None
