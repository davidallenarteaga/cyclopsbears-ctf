<div class="jumbotron">
    <h1 class="cybeartron preslash postslash">Rules</h1>
</div>

##CTF Overview

"The Fall of Cybeartron" is a Jeopardy style Capture-The-Flag competition held
at the BSides Canberra 2019 conference. This event is designed to be fun and
educational. To get the most enjoyment out of this event, try to remember this.

##Registration and logistics

The CTF will be open for registration from 1000 on Friday 15 March 2019 until
1530 on Saturday 16 March 2019. 

To register an individual and team, click [here](register).

The CTF will be online between 1000-1730 Friday 15 March, and 0900-1530
Saturday 16 March. Contestants may download challenges to play offline, but
will not be able to submit flags, or interact with online services overnight.

##Challenge scoring and flags

Unlike some other CTFs that have static scores for challenges, this CTF will
have "Dynamic Value" or "Market value" scoring__^__. This means that all challenges
will start with the same value. As more teams solve the challenge, the score
will decay down to a minimum value. This will effect teams that have already
solved the challenge: you may want to consider whether you try to solve an
already solved challenge to reduce another teams score while increasing yours,
or whether you try to solve an unsolved challenge to get the maximum score!

Flags will generally be of the form [cybears{flagflagflag}](scoreboard) unless
specified otherwise.

Some challenges may unlock during the competition; if you have checked out what
is available on the Friday, it'll pay to look again on the Saturday. We'll
likely issue a notification when new challenges are released.

The team with the maximum score at 1530 on Saturday 16 March 2019 will be the
winner of the BSides CTF 2019.

##Help!

The Cybears team are here to help and will be manning the admin booth in the
CTF room. Your fellow competitors may also be a good source of advice, and CTFs
are a friendly environment, but flags must be earned - not given.

If hints are provided by the admins, every effort will be made to provide them
to all competitors at the same time.

##Prizes

1st prize : $1000
2nd prize : $500
3rd prize : $250

Certain challenges have a [trophy](challenges) tag: the first team to solve
these challenges will win a small trophy, a Raspberry Pi and a T-Shirt.

Contestants **must** be present at the BSides closing ceremony to receive the
main prizes.


##Rules

- You may not do anything to diminish the competition experience for other
  players on the system; if you are worried that you might break something, ask
  an admin.
- No bullying or harrassment of any kind (online or in person).
- Do not target or hack other contestants.
- Do not target or hack the registration or scoring infrastructure
  (ctf.cybears.io).
- There are no challenges that require you to scan or brute force the
  infrastructure. Please don't do this. If you think a challenge requires it,
  please come and talk to us. If you believe you have accidentally DOS'd the
  infrastructure, please let us know.
- The Cybears admins have the authority to ban or disqualify contestants that
  do not abide by the rules, or significantly detract from the enjoyment of
  other contestants, admins or conference attendees.
- The Cybears admins decisions are final.
- In the rare event of a tie for prizes, the tied prizes will be pooled and
  then halved. For a 3rd place tie, the prize pool will be split.
- You may be connecting to an untrusted system or systems. While the Cybears
  team has made every effort to create a stable and safe challenge environment,
  they are ultimately not responsible for any damage to contestants computers
  or equipment. You should take adequate precautions (eg. connecting from a
  virtual machine).

### __^__Scoring Algorithm: 

*(initial - min) &ast; math.exp(-num_solves &ast; math.log(2) / decay) + min*

* Regular challenges: Initial is 500, minimum is 50, decay is 4.
* Trivia challenges: Initial is 100, minimum is 10, decay is 4. 
