/**
 * @author alteredq / http://alteredqualia.com/
 * @author mr.doob / http://mrdoob.com/
 */

var WEBGL = {

	isWebGLAvailable: function () {

		try {

			var canvas = document.createElement( 'canvas' );
			return !! ( window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) ) );

		} catch ( e ) {

			return false;

		}

	},

	isWebGL2Available: function () {

		try {

			var canvas = document.createElement( 'canvas' );
			return !! ( window.WebGL2RenderingContext && canvas.getContext( 'webgl2' ) );

		} catch ( e ) {

			return false;

		}

	},

	getWebGLErrorMessage: function () {

		return this.getErrorMessage( 1 );

	},

	getWebGL2ErrorMessage: function () {

		return this.getErrorMessage( 2 );

	},

	getErrorMessage: function ( version ) {

		var names = {
			1: 'WebGL',
			2: 'WebGL 2'
		};

		var contexts = {
			1: window.WebGLRenderingContext,
			2: window.WebGL2RenderingContext
		};

		// var message = 'Your $0 does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">$1</a>';
		var message = 'Your $0 does not seem to support WebGL. Go here to fix: http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation';

		// var element = document.createElement( 'div' );
		// element.id = 'webglmessage';
		// element.style.fontFamily = 'monospace';
		// element.style.fontSize = '13px';
		// element.style.fontWeight = 'normal';
		// element.style.textAlign = 'center';
		// element.style.background = '#fff';
		// element.style.color = '#000';
		// element.style.padding = '1.5em';
		// element.style.width = '400px';
		// element.style.margin = '5em auto 0';

		if ( contexts[ version ] ) {

			message = message.replace( '$0', 'graphics card' );

		} else {

			message = message.replace( '$0', 'browser' );

		}

		message = message.replace( '$1', names[ version ] );

		// element.innerHTML = message;
		console.log(message);

		// return element;
		return message;

	}

};

// Add debug model to test scale / performance
// Can remove when code is pushed live

function load_test() {

    testloader.load( 'themes/cybears/static/models/test.stl', function ( geometry ) {

        var material = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0xffff00, shininess: 200 } );
		var test = new THREE.Mesh( geometry, material );

        test.position.set( 1250, -70, 0 );
        test.rotation.set( Math.PI / 2, Math.PI, 0 );
        test.scale.set( 25, 25, 25 );

        test.castShadow = true;
        test.receiveShadow = true;

		scene.add( test );
		hooked = false;

    } );

}
