# Cybears CTFd Challenge page with tag filtering

This is a React component for the CTFd challenges page which adds dynamically generated filters, and the ability to filter
challenges based on their tags.

The script `postbuild.sh` will run after an `npm run build`. `postbuild.sh` copies the generated static javascript
files over to the `../extras/themes/cybears/static/js/` which is then mounted into a docker-compose'd CTFd
container. It also has some ugly `sed` commands to strip out the newly named chunk files, which gets embedded into
the `../extras/themes/cybears/templates/cybears-challenges.html` file.

The `cybears-challenges.html` file is automatically included into the `../extras/themes/cybears/templates/challenges.html`
Jinja template. Super ugly, but it avoids all of the webpack collisions from trying to work with the CTFd mess of dependencies.

Would definitely appreciate some help on the dev workflow for this mess.
