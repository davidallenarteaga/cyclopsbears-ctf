const Tag = props => {
    const { value, onClick, additionalClasses } = props
    const classes = ["filter-tag"].concat(additionalClasses)

    return (
        <button className={classes.join(' ')} onClick={onClick}>{value}</button>
    )
}

export default Tag
