import ChallengeTags from './ChallengeTags'

const Challenge = props => {
    const { challenge, onClick } = props
    const classes = ["single-challenge"]
    if (challenge.solved_by_me) classes.push("solved-challenge")

    let icon = <i className="fas fa-tint-slash"></i>
    if (challenge.solves === 0) {
        classes.push("first-blood")
        icon = <i className="fas fa-tint"></i>
    }

    if (challenge.type === "hidden") {
        icon = <i className="fas fa-lock"></i>
        classes.push("hidden-challenge")
    }

    return (
        <div className={classes.join(' ')} onClick={onClick}>
            <h2>{challenge.name}</h2>
            {icon}
            <p>{challenge.value}</p>
            <ChallengeTags tagsArray={challenge.tags.map(tag => tag.value)} />
        </div>
    )
}

export default Challenge
