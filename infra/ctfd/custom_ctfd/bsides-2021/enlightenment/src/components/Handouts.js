const Handouts = props => {
    const { files } = props

    if (files.length === 0) return null

    return files.map(file => {
        const filename = file.split("/").pop().split("?")[0]
        return <a key={filename} className="handout" href={file}><i class="fas fa-download"></i>{filename}</a>
    })
}

export default Handouts
