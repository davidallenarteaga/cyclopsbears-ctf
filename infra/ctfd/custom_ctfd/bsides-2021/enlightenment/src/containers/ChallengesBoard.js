import { useState, useEffect, useCallback } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import Alert from 'react-bootstrap/Alert'

import FilterControls from '../components/FilterControls'
import Challenges from '../components/Challenges'
import ChallengeModal from './ChallengeModal'
import { baseURL } from '../baseUrl'

const ChallengesBoard = props => {
  const [challenges, setChallenges] = useState(null)
  const [tags, setTags] = useState([])
  const [includeFilters, setIncludeFilters] = useState([])
  const [excludeFilters, setExcludeFilters] = useState([])
  const [activeChallengeId, setActiveChallengeId] = useState(null)
  const [modalShow, setModalShow] = useState(false)
  const [sortByPoints, setSortByPoints] = useState(false)
  const [sortDescending, setSortDescending] = useState(false)
  const [search, setSearch] = useState("")

  const getChallenges = useCallback(() => {
    fetch(`${baseURL}/api/v1/challenges`)
      .then(response => response.json())
      .then(result => {
        setChallenges(result.data)

        const tagsArray = result.data.map(obj => obj.tags)
          .filter(tagArray => tagArray.length > 0)
          .flat()
          .map(tag => tag.value)

        setTags(tagsArray)
      })
      .catch(error => {
        console.error(`Challenges couldn't be loaded: `, error)
        return <Alert variant="danger" show={true}>Oh no! {error.message}. Please refresh.</Alert>
      })
  }, [])

  useEffect(() => {
    getChallenges()
    const timer = setInterval(() => { getChallenges() }, 30 * 1000)
    return () => clearInterval(timer)
  }, [getChallenges])

  const handleTagClick = tag => {
    // State machine for filters looks like:
    // 1. Not in a filter list
    // 2. Moved into include filter list
    // 3. Moved into the exclude filter list
    // 4. Removed from filter list

    if (includeFilters.includes(tag)) {
      // 3. Move tag from include -> exclude list
      setIncludeFilters(includeFilters.filter(f => f !== tag))
      setExcludeFilters(excludeFilters.concat(tag))
    }
    else if (excludeFilters.includes(tag)) {
      // 4. Remove tag from filter lists
      setExcludeFilters(excludeFilters.filter(f => f !== tag))
    }
    else {
      // 2. Move tag into include filter list
      setIncludeFilters(includeFilters.concat(tag))
    }
  }

  const clearFilters = () => {
    setIncludeFilters([])
    setExcludeFilters([])
    setSearch("")
  }

  const handleActivateChallenge = id => {
    setActiveChallengeId(id)
    setModalShow(true)
  }

  const handleHideModal = () => {
    setActiveChallengeId(null)
    setModalShow(false)
  }

  const visibleChallenges = challenges ? challenges.filter(challenge => {
    const name = challenge.name.toLowerCase()

    return name.includes(search.toLowerCase())
  }) : null

  return (
    <>
      <FilterControls
        tags={tags}
        includeFilters={includeFilters}
        excludeFilters={excludeFilters}
        handleTagClick={handleTagClick}
        search={search}
        sortByPoints={sortByPoints}
        sortDescending={sortDescending}
        setSortByPoints={setSortByPoints}
        setSortDescending={setSortDescending}
        setSearch={setSearch}
        clearFilters={clearFilters}
      />

      <div className="challenge-container">
        {challenges ?
          <Challenges
            challenges={visibleChallenges}
            includeFilters={includeFilters}
            excludeFilters={excludeFilters}
            sortByPoints={sortByPoints}
            sortDescending={sortDescending}
            activateChallenge={handleActivateChallenge}
          /> : <Spinner animation="border" />}
      </div>
      <div className="challenge-modal">
        {activeChallengeId &&
          <ChallengeModal
            id={activeChallengeId}
            show={modalShow}
            onHide={handleHideModal}
            solveHandler={getChallenges}
          />
        }
      </div>
    </>
  )
}

export default ChallengesBoard
