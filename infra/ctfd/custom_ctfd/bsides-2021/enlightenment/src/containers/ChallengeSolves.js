import { useState, useLayoutEffect } from 'react'

import { baseURL } from '../baseUrl'

const ChallengeSolves = props => {
    // passing in solved as a prop to force re-render
    const { id, solved, isHidden } = props
    const [solves, setSolves] = useState(null)

    useLayoutEffect(() => {
        fetch(`${baseURL}/api/v1/challenges/${id}/solves`)
            .then(response => response.json())
            .then(result => {
                setSolves(result.data)
            })
            .catch(console.error)
    }, [id, solved])

    let output = <p>One must follow the path of enlightenment to see solves for this challenge.</p>

    if (!isHidden) {
        output = <p>No solves yet!</p>
        if (solves && solves.length) {
            output = solves.map(solve => <a key={solve.name} className="solve-link" href={solve.account_url}>{solve.name}</a>)
        }
    }

    return <div className="solves">{output}</div>
}

export default ChallengeSolves
