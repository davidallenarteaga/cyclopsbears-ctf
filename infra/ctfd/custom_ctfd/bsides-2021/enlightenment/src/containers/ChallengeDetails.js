import { useEffect, useState } from 'react'
import ReactMarkdown from 'react-markdown'
import Alert from 'react-bootstrap/Alert'

import { baseURL } from '../baseUrl'
import SubmissionAlert from '../components/SubmissionAlert'
import Handouts from '../components/Handouts'
import FlagSubmission from '../components/FlagSubmission'
import ChallengeTags from '../components/ChallengeTags'

const ChallengeDetails = props => {
    const { challenge, onSolve, isHidden } = props
    const [flag, setFlag] = useState('')
    const [csrfNonce, setCsrfNonce] = useState(null)
    const [attemptResult, setAttemptResult] = useState(null)
    const [showAlert, setShowAlert] = useState(false)

    useEffect(() => {
        setCsrfNonce(document.querySelector("#csrfNonce").innerHTML)
        const timer = setTimeout(() => setShowAlert(false), 3000)
        return () => clearTimeout(timer)
    }, [showAlert, setShowAlert])

    const handleAttempt = result => {
        setAttemptResult(result)
        setShowAlert(true)
        if (result.data.status === "correct") onSolve()
    }

    const handleSubmit = event => {
        event.preventDefault()
        setShowAlert(false)
        const data = { challenge_id: challenge.id, submission: flag }
        const url = `${baseURL}/api/v1/challenges/attempt`
        const headers = new Headers()
        headers.append("Accept", "application/json")
        headers.append("Content-Type", "application/json")
        headers.append("CSRF-Token", csrfNonce)
        const requestOptions = {
            method: 'POST',
            credentials: 'same-origin',
            headers: headers,
            body: JSON.stringify(data),
            redirect: 'follow'
        }

        fetch(url, requestOptions)
            .then(response => response.json())
            .then(result => {
                handleAttempt(result)
            })
            .catch(error => {
                setAttemptResult(null)
                setShowAlert(false)
                console.error(error.message)
            })
    }

    const handleChange = event => setFlag(event.target.value)

    let output = null
    if (challenge) {
        let footer = <Alert variant="success" show>Solved <i className="fas fa-check"></i></Alert>
        if (!challenge.solved_by_me) {
            footer = showAlert ?
                <SubmissionAlert
                    attempt={attemptResult}
                    show={showAlert}
                    close={() => setShowAlert(false)} />
                : <FlagSubmission
                    handleSubmit={event => handleSubmit(event)}
                    handleChange={event => handleChange(event)} />
        }

        output = (
            <>
                <h4>Challenge Hidden!</h4>
                <p>You haven't unlocked this challenge yet!</p>
                <div className="details-footer">
                    <Alert variant="warning" show>Hidden</Alert>
                </div>
            </>
        )

        if (!isHidden) {
            output = (
                <>
                    <h4>{challenge.name}</h4>
                    <ChallengeTags tagsArray={challenge.tags} />
                    <ReactMarkdown>{challenge.description}</ReactMarkdown>
                    <h5>Points: {challenge.value}</h5>
                    <Handouts files={challenge.files} />
                    <div className="details-footer">{footer}</div>
                </>
            )
        }
    }

    return output
}

export default ChallengeDetails
