import { group, sleep, check, fail } from 'k6';
import http from 'k6/http';
import { Rate } from "k6/metrics";

// Version: 1.2
// Creator: WebInspector


export let errorRate = new Rate("errors");

export let options = {
	maxRedirects: 0,
	thresholds: {
		errors: ["rate<0.1"] // <10% errors
	  }
};

let host =  `${__ENV.HOSTNAME}`;
let origin = "http://" + host;
let base_url = origin + "/";
let submissions = [
	{id: 1, answer: "cybears{My_DiSk_Is_My_0-1_KnAPS@cK}"},
	{id: 8, answer: "cybears{m0n0chr0m3}"},
	{id: 10, answer: "cybears{2147483647}"}
]



export default function() {
	let username = Math.random().toString(36).substring(2);
	let newteam = Math.random().toString(36).substring(2);
	let password = Math.random().toString(36).substring(7);
	let session = "";
	let nonce = "";
	let team_nonce = "";
	let io = "";
	let sid = "";
	let counter = 0;
	group("Get vars", function() {

		let res = http.get(base_url,{
            "headers": {
                "Host": host,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
                "Sec-Fetch-Dest": "document",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-User": "?1",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9"
            }
		});
		if (check(res, {
			'get status was 200': res => res.status === 200,
		})) {
			session = res.cookies['session'];
			nonce = res.body.split('var csrf_nonce = "')[1].split('"')[0];
		} else {
			fail("status code was *not* 200");
		}
		
		res = http.get(base_url + "socket.io/?EIO=3&transport=polling&t=" + Date.now() + "-" + counter++, {
			"cookies": {
				"session": session
			},
			"headers": {
				"Host": host,
				"Connection": "keep-alive",
				"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
				"Sec-Fetch-Dest": "empty",
				"Accept": "*/*",
				"Sec-Fetch-Site": "same-origin",
				"Sec-Fetch-Mode": "cors",
				"Referer": base_url,
				"Accept-Encoding": "gzip, deflate, br",
				"Accept-Language": "en-US,en;q=0.9"
			}
		});
		if (check(res, {
			'get socket.io status was 200': res => res.status === 200,
		})) {
			io = res.cookies['io'];
            sid = io[0]['value'];
		} else {
			fail("get socket.io status code was *not* 200");
		}

	});

	group("page_1 - load homepage", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-fonts.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/three.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/WebGL.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/postprocessing/EffectComposer.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/postprocessing/RenderPass.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/postprocessing/ShaderPass.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/shaders/CopyShader.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/shaders/LuminosityHighPassShader.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/postprocessing/UnrealBloomPass.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/loaders/STLLoader.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/cybeartron.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/3d/loaders/GLTFLoader.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"sec-fetch-dest": "style",
					"accept": "text/css,*/*;q=0.1",
					"sec-fetch-site": "cross-site",
					"sec-fetch-mode": "no-cors",
					"referer": base_url,
					"accept-encoding": "gzip, deflate, br",
					"accept-language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"sec-fetch-dest": "style",
					"accept": "text/css,*/*;q=0.1",
					"sec-fetch-site": "cross-site",
					"sec-fetch-mode": "no-cors",
					"referer": base_url,
					"accept-encoding": "gzip, deflate, br",
					"accept-language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_1 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(0.98);
		req = [{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "themes/cybears/static/css/cybears.css",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/models/decepticomTSS.gltf",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "empty",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/models/cybears.gltf",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "empty",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "themes/cybears/static/css/cybears.css",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			if(!check(r, {
				"page_1-1 get code was 200": (res) => res.status == 200 || res.status == 302
			})) {
				console.log(r.request.url + " failed with " + r.status)
				errorRate.add(1);
			}
		});
		sleep(0.98);
		req = [{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "themes/cybears/static/css/cybears.css",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "themes/cybears/static/css/cybears.css",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_1-3 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(1.26);
	});
	group("page_2 - load register page", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": base_url + "register",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url,
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-fonts.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "register",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "register",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_2 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(12.34);
	});
	group("page_3 - register user", function() {
		let req, res;
		req = [{
			"method": "post",
			"url": base_url + "register",
			"body": {
				"name": username,
				"email": username + "@mail.com",
				"password": password,
				"nonce": nonce
			},
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Cache-Control": "max-age=0",
					"Origin": origin,
					"Upgrade-Insecure-Requests": "1",
					"Content-Type": "application/x-www-form-urlencoded",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "register",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Cache-Control": "max-age=0",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "register",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "team?next=%2Fchallenges%3F",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Cache-Control": "max-age=0",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "register",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-fonts.css",
			"params": {
				"headers": {
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font.css",
			"params": {
				"headers": {
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/plotly.min.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "team?next=%2Fchallenges%3F",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		}];
		let responses = http.batch(req);
		console.log("Created user " + username);

		const result  = check(responses[0], {
			'registration status was 302': res => res.status === 302,
		  });
		  errorRate.add(!result);

		responses.slice(1).forEach(r => {
			if(!check(r, {
				"page_3 get code was 200": (res) => res.status == 200 || res.status == 302
			})) {
				errorRate.add(1);
			}
		});
		sleep(2.01);
	});
	group("page_4 - load create team page", function() {

		let get_teams_res = http.get(base_url + "teams/new", {
			"cookies": {
				"session": session
			},
			"headers": {
				"Host": host,
				"Connection": "keep-alive",
				"Upgrade-Insecure-Requests": "1",
				"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
				"Sec-Fetch-Dest": "document",
				"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
				"Sec-Fetch-Site": "same-origin",
				"Sec-Fetch-Mode": "navigate",
				"Sec-Fetch-User": "?1",
				"Referer": base_url + "team?next=%2Fchallenges%3F",
				"Accept-Encoding": "gzip, deflate, br",
				"Accept-Language": "en-US,en;q=0.9"
			}
		});

		if (check(get_teams_res, {
			'get new team page status was 200': res => res.status === 200,
		})) {
			team_nonce = get_teams_res.body.split('var csrf_nonce = "')[1].split('"')[0]
		} else {
			fail("get new team page status was *not* 200");
		}

		let req, res;
		req = [{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-fonts.css",
			"params": {
				"headers": {
					"Referer": base_url + "teams/new",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font.css",
			"params": {
				"headers": {
					"Referer": base_url + "teams/new",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "teams/new",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "teams/new",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_4 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(5.18);
	});
	group("page_5 - create team", function() {
		let req, res;
		req = [{
			"method": "post",
			"url": base_url + "teams/new",
			"body": {
				"name": newteam,
				"password": password,
				"nonce": team_nonce
			},
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Cache-Control": "max-age=0",
					"Origin": origin,
					"Upgrade-Insecure-Requests": "1",
					"Content-Type": "application/x-www-form-urlencoded",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "teams/new",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Cache-Control": "max-age=0",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "teams/new",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-fonts.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/challenge-board.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/hide-footer.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/multi-modal.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/challenges.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/hints.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/webfonts/fa-solid-900.woff2",
			"params": {
				"headers": {
					"origin": origin,
					"sec-fetch-dest": "font",
					"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"accept": "*/*",
					"sec-fetch-site": "cross-site",
					"sec-fetch-mode": "cors",
					"referer": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
					"accept-encoding": "gzip, deflate, br",
					"accept-language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];

		let responses = http.batch(req);

		if(!check(responses[0], {
			'team registration status was 302': res => res.status === 302,
		  })) {
			  fail("team registration failed");
		  }


		responses.slice(1).forEach(r => {
			if(!check(r, {
				"page_5 get code was 200": (res) => res.status == 200 || res.status == 302
			})) {
				errorRate.add(1);
				console.log("page_5 - " + r.request.url + " failed: " + r.status)
			}
		});

		sleep(3.81);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/7",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584313682022",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/grid.png",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "themes/cybears/static/css/base.css",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_5-1 get code was 200 or 403": (res) => res.status == 200 || res.status == 403
			});
			errorRate.add(!result);
		});
		sleep(1.40);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/9",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584313682023",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_5-2 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(1.01);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/6",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584313682024",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "themes/cybears/static/css/cybears.css",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_5-3 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(1.05);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/10",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584313682025",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_5-4 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(5.96);
	});
	group("page_6 - list challenges", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": base_url + "challenges",
			"params": {
				"cookies": {
					"session": session,
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Cache-Control": "max-age=0",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/challenges.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/multi-modal.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/hints.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/webfonts/fa-solid-900.woff2",
			"params": {
				"headers": {
					"Referer": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/favicon.ico",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Pragma": "no-cache",
					"Cache-Control": "no-cache",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/favicon.ico",
			"params": {
				"headers": {
					"Referer": base_url + "challenges",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/challenges/10",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584326999954",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/grid.png"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_6 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(24.40);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_6-4 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(3.11);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/1",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584326999955",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_6-5 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(2.65);
		
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			if(!check(r, {
				"page_6-6 get code was 200 or 302": (res) => res.status == 200 || res.status == 302
			})) {
				errorRate.add(1);
			}
		});
		sleep(13.35);

		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/8",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584326999956",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_6-11 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(5.89);
	});
	group("page_7 - show scoreboard", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": base_url + "scoreboard",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-fonts.css",
			"params": {
				"headers": {
					"Referer": base_url + "scoreboard",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font.css",
			"params": {
				"headers": {
					"Referer": base_url + "scoreboard",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "scoreboard",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/plotly.min.js",
			"params": {
				"headers": {
					"Referer": base_url + "scoreboard",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/scoreboard.js",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script",
					"Accept": "*/*",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "scoreboard",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/webfonts/fa-solid-900.woff2",
			"params": {
				"headers": {
					"Referer": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		},{
			"method": "get",
			"url": base_url + "api/v1/scoreboard/top/10",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "scoreboard",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_7 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(7.94);
		req = [{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_7-1 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(1.99);
	});
	group("page_8 - solve challenges", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": base_url + "challenges",
			"params": {
				"cookies": {
					"session": session,
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Cache-Control": "max-age=0",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/challenges.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/multi-modal.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/hints.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/webfonts/fa-solid-900.woff2",
			"params": {
				"headers": {
					"Referer": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/favicon.ico",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Pragma": "no-cache",
					"Cache-Control": "no-cache",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image",
					"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/favicon.ico",
			"params": {
				"headers": {
					"Referer": base_url + "challenges",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/challenges/10",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584326999954",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/grid.png"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_8 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(24.40);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/teams/me/solves",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_8-4 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(3.11);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/1",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584326999955",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_8-5 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(2.65);


		submissions.forEach( submission => {
			req = req = [{
				"method": "post",
				"url": base_url + "api/v1/challenges/attempt",
				"body": "{\"challenge_id\":" + submission['id'] + ",\"submission\":\"" + submission['answer'] + "\"}",
				"params": {
					"cookies": {
						"session": session
					},
					"headers": {
						"Host": host,
						"Connection": "keep-alive",
						"Accept": "application/json",
						"CSRF-Token": team_nonce,
						"Sec-Fetch-Dest": "empty",
						"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
						"Content-Type": "application/json",
						"Origin": origin,
						"Sec-Fetch-Site": "same-origin",
						"Sec-Fetch-Mode": "cors",
						"Referer": base_url + "challenges",
						"Accept-Encoding": "gzip, deflate, br",
						"Accept-Language": "en-US,en;q=0.9"
					}
				}
			},{
				"method": "get",
				"url": base_url + "api/v1/challenges",
				"params": {
					"cookies": {
						"session": session
					},
					"headers": {
						"Host": host,
						"Connection": "keep-alive",
						"Accept": "*/*",
						"Sec-Fetch-Dest": "empty",
						"X-Requested-With": "XMLHttpRequest",
						"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
						"Sec-Fetch-Site": "same-origin",
						"Sec-Fetch-Mode": "cors",
						"Referer": base_url + "challenges",
						"Accept-Encoding": "gzip, deflate, br",
						"Accept-Language": "en-US,en;q=0.9"
					}
				}
			},{
				"method": "get",
				"url": base_url + "api/v1/teams/me/solves",
				"params": {
					"cookies": {
						"session": session
					},
					"headers": {
						"Host": host,
						"Connection": "keep-alive",
						"Accept": "*/*",
						"Sec-Fetch-Dest": "empty",
						"X-Requested-With": "XMLHttpRequest",
						"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
						"Sec-Fetch-Site": "same-origin",
						"Sec-Fetch-Mode": "cors",
						"Referer": base_url + "challenges",
						"Accept-Encoding": "gzip, deflate, br",
						"Accept-Language": "en-US,en;q=0.9"
					}
				}
			}];
			res = http.batch(req);
	
			res.forEach(r => {
				if(!check(r, {
					"submission was 200": (res) => res.status == 200
				})) {
					errorRate.add(1);
					fail('Failed to submit flag')
				}
			});
			sleep(13.35);

		});
		res = http.post(base_url + "api/v1/challenges/attempt", "{\"challenge_id\":1,\"submission\":\"sadfasdfsdf\"}", {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "application/json",
					"CSRF-Token": team_nonce,
					"Sec-Fetch-Dest": "empty",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Content-Type": "application/json",
					"Origin": origin,
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			});
		const result = check(res, {
			"incorrect submission was 200": (res) => res.status == 200
		});
		if(!result) {
			console.log("incorrect submission " + res.status);
		}
		errorRate.add(!result);
		sleep(13.35);
		req = [{
			"method": "get",
			"url": base_url + "api/v1/challenges/8",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.js?_=1584326999956",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "plugins/dynamic_challenges/assets/view.html",
			"params": {
				"headers": {
					"Accept": "*/*",
					"Referer": base_url + "challenges",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			const result = check(r, {
				"page_8-11 get code was 200": (res) => res.status == 200 || res.status == 302
			});
			errorRate.add(!result);
		});
		sleep(21.89);
	});
	group("page_9 - show scoreboard", function() {
		let req, res;
		req = [{
			"method": "get",
			"url": base_url + "scoreboard",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Upgrade-Insecure-Requests": "1",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "document",
					"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "navigate",
					"Sec-Fetch-User": "?1",
					"Referer": base_url + "challenges",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bootstrap.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-fonts.css",
			"params": {
				"headers": {
					"Referer": base_url + "scoreboard",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font-awesome/fontawesome-all.min.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/font.css",
			"params": {
				"headers": {
					"Referer": base_url + "scoreboard",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/fetch.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/sticky-footer.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/cybears.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/base.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "static/user.css",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "style",
					"Accept": "text/css,*/*;q=0.1",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "no-cors",
					"Referer": base_url + "scoreboard",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/promise-polyfill.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/jumbotron.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/core/static/js/CTFd.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/moment.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/nunjucks.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/socket.io.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-logo.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/img/asd-code.png",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "image"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/jquery.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/events.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/bootstrap.bundle.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/style.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/markdown-it.min.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/ezq.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/utils.js",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/scoreboard.js",
			"params": {
				"headers": {
					"Referer": base_url + "scoreboard",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/js/vendor/plotly.min.js",
			"params": {
				"headers": {
					"Referer": base_url + "scoreboard",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Dest": "script"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Raleway:400,400i,700,700i&subset=latin-ext",
			"params": {
				"headers": {
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Referer": base_url + "scoreboard",
					"Sec-Fetch-Dest": "style"
				}
			}
		},{
			"method": "get",
			"url": "https://use.fontawesome.com/releases/v5.4.1/webfonts/fa-solid-900.woff2",
			"params": {
				"headers": {
					"Referer": "https://use.fontawesome.com/releases/v5.4.1/css/all.css",
					"Origin": origin,
					"Sec-Fetch-Dest": "font",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
				}
			}
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/3270Narrow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Megatron-Italic.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/OptimusHollow.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Cybertron.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/fonts/Optimus.woff"
		},{
			"method": "get",
			"url": base_url + "themes/cybears/static/css/vendor/bullet.png"
		},{
			"method": "get",
			"url": base_url + "api/v1/scoreboard/top/10",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "scoreboard",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		},{
			"method": "get",
			"url": base_url + "api/v1/scoreboard",
			"params": {
				"cookies": {
					"session": session
				},
				"headers": {
					"Host": host,
					"Connection": "keep-alive",
					"Accept": "*/*",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36",
					"Sec-Fetch-Site": "same-origin",
					"Sec-Fetch-Mode": "cors",
					"Referer": base_url + "scoreboard",
					"Accept-Encoding": "gzip, deflate, br",
					"Accept-Language": "en-US,en;q=0.9"
				}
			}
		}];
		res = http.batch(req);

		res.forEach(r => {
			if(!check(r, {
				"page_8-13 get code was 200": (res) => res.status == 200 || res.status == 302
			})) {
				errorRate.add(1);
			}
		});
		// Random sleep between 20s and 40s
		sleep(Math.floor(Math.random()*20+20));
	});

}
