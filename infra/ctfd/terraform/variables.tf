variable "aws_region" {
  default = "ap-southeast-2"
}

variable "ctfd_eks_cluster_name" {
  description = "CTFd eks cluster name"
}

# eks autoscaling
variable "ctfd_eks_autoscaling_group_min_size" {
  default = 1
}

variable "ctfd_eks_autoscaling_group_desired_capacity" {
  default = 1
}

variable "ctfd_eks_autoscaling_group_max_size" {
  default = 2
}

variable "ctfd_eks_instance_type" {
  default = "m5.large"
}
# Elasticcahe - redis configuration
variable "elasticache_cluster_instances" {
  type        = number
  description = "Number of instances in ElastiCache cluster"
  default     = 3
}

variable "elasticache_cluster_instance_type" {
  type        = string
  description = "Instance type for instance in ElastiCache cluster"
  default     = "cache.m6g.large"
}


# RDS configuration
variable "db_cluster_instances" {
  type        = number
  description = "Number of instances to create in the RDS cluster. Only used if db_engine_mode set to `provisioned`"
  default     = 1
}
variable "db_engine_mode" {
  type        = string
  description = "Engine mode the RDS cluster, can be `provisioned` or `serverless`"
  default     = "serverless"
}

variable "db_cluster_instance_type" {
  type        = string
  description = "Type of instances to create in the RDS cluster. Only used if db_engine_mode set to `provisioned`"
  default     = "db.r5.large"
}
variable "db_serverless_min_capacity" {
  type        = number
  description = "Minimum capacity for serverless RDS. Only used if db_engine_mode set to `serverless`"
  default     = 1
}

variable "db_serverless_max_capacity" {
  type        = number
  description = "Maximum capacity for serverless RDS. Only used if db_engine_mode set to `serverless`"
  default     = 128
}

variable "db_deletion_protection" {
  type        = bool
  description = "If true database will not be able to be deleted without manual intervention"
  default     = true
}

variable "force_destroy_challenge_bucket" {
  type        = bool
  default     = false
  description = "Whether the S3 bucket containing the CTFD challenge data should be force destroyed"
}

# Frontend autoscaling
variable "asg_min_size" {
  type        = number
  description = "Minimum of instances in frontend auto scaling group"
  default     = 1
}

variable "asg_max_size" {
  type        = number
  description = "Maximum of instances in frontend auto scaling group"
  default     = 1
}

variable "asg_instance_type" {
  type        = string
  description = "Type of instances in frontend auto scaling group"
  default     = "t3a.micro"
}

# gunicorn variables
variable "workers" {
  type        = number
  description = "Number of workers (processes) for gunicorn. Should be (CPU's *2) + 1) based on CPU's from asg_instance_type"
  default     = 5
}

variable "worker_connections" {
  type        = number
  description = "Number of worker connections (pseudo-threads) per worker for gunicorn. Should be (CPU's *2) + 1) * 1000. based on CPU's from asg_instance_type"
  default     = 5000
}

variable "https_certificate_arn" {
  description = "SSL Certificate ARN to be used for the HTTPS server. If empty a certificate will be created."
}

variable "ctf_domain" {
  default = "ctf.cybears.io"
}

variable "ctf_domain_zone_id" {
  description = "zone id for the route53 zone for the ctf_domain"
}

variable "ctfd_theme" {
  default     = "core"
  description = "What theme to use for CTFd"
}

variable "ctfd_version" {
  description = "Version of CTFd to deploy"
}

variable "ctfd_dir" {
  description = "Directory of CTFd modifications to apply"
}

variable "allowed_cidr_blocks" {
  type        = list
  description = "Cidr blocks allowed to hit the frontend (ALB)"
  default     = ["0.0.0.0/0"]
}

variable "ctfd_repo" {
  type        = string
  description = "Git repository to clone CTFd from"
  default     = "https://github.com/CTFd/CTFd.git"
}

variable "ctfd_overlay" {
  type        = string
  default     = "most/certainly/does/not/exist"
  description = "Path to compressed package to unpack over the top of the CTFd repository. Used to package custom themes and plugins. Must be a gzip compressed tarball"
}

variable "db_skip_final_snapshot" {
  type        = bool
  description = "If true database will not be snapshoted before deletion."
  default     = false
}