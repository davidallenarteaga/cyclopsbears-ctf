import os
import subprocess
import json

TERRAFORM_PROG=['terraform']

def output(base_dir, output_variable='', json_output=True):
    init(base_dir)
    args = ['output']
    if json_output:
        args.append('--json')
    args.append(output_variable)
    result = subprocess.check_output(TERRAFORM_PROG + args, cwd=base_dir)
    if b'No outputs found' in result:
        raise subprocess.CalledProcessError(b'No outputs found')
    return json.loads(result) if json_output else result[6:-5] #strip <<EOT\n and EOT\n

def init(base_dir):
    subprocess.check_call(TERRAFORM_PROG + ['init', '-upgrade'], cwd=base_dir)

def apply(base_dir, var_file='', vars=None, verbose=False, sudo=False):
    init(base_dir)
    proc = ["sudo"] + TERRAFORM_PROG if sudo else TERRAFORM_PROG
    
    params = ['apply', '-auto-approve']
    if vars:
        for var, value in vars.items():
            params.append('-var')
            params.append(var + '=' + value)
    if var_file:
        params.append('-var-file=' + var_file)

    env = {**os.environ, "TF_LOG": "TRACE"} if verbose else {**os.environ}
    subprocess.check_call(proc + params, cwd=base_dir, env=env)
    
def destroy(base_dir, var_file='', vars=None, verbose=False, sudo=False):
    proc = ["sudo"] + TERRAFORM_PROG if sudo else TERRAFORM_PROG
    params = ['destroy', '-auto-approve',]
    if vars:
        for var, value in vars.items():
            params.append('-var')
            params.append(var + '=' + value)
    if var_file:
        params.append('-var-file=' + var_file)

    env = {**os.environ, "TF_LOG": "TRACE"} if verbose else {**os.environ}

    subprocess.check_call(proc + params, cwd=base_dir, env=env)

def state_pull(base_dir):
    init(base_dir)
    
    params = ['state', 'pull']
    subprocess.check_call(TERRAFORM_PROG + params, cwd=base_dir)

def refresh(base_dir, var_file='', vars=None, verbose=False, sudo=False):
    proc = ["sudo"] + TERRAFORM_PROG if sudo else TERRAFORM_PROG
    params = ['refresh']
    if vars:
        for var, value in vars.items():
            params.append('-var')
            params.append(var + '=' + value)
    if var_file:
        params.append('-var-file=' + var_file)

    env = {**os.environ, "TF_LOG": "TRACE"} if verbose else {**os.environ}

    subprocess.check_call(proc + params, cwd=base_dir, env=env)